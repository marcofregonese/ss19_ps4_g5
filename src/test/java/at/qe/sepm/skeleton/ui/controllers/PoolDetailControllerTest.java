package at.qe.sepm.skeleton.ui.controllers;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.GameMode;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.AnswerStatisticService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class PoolDetailControllerTest {

	//@Autowired
	//PoolDetailController poolDetailController;

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void question123Test() {
		PoolDetailController poolDetailController = new PoolDetailController();
		Question question = new Question();
		question.setQuestion("Tester");
		poolDetailController.setQuestion123(question);
		Assert.assertEquals(question, poolDetailController.getQuestion123());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void testTest() {
		PoolDetailController poolDetailController = new PoolDetailController();
		poolDetailController.setTest("Test");
		Assert.assertEquals("Test", poolDetailController.getTest());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void testerTest() {
		PoolDetailController poolDetailController = new PoolDetailController();
		poolDetailController.setTester("Test");
		Assert.assertEquals("Test", poolDetailController.getTester());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void questionTest() {
		PoolDetailController poolDetailController = new PoolDetailController();
		Question question = new Question();
		question.setQuestion("Tester");
		poolDetailController.setQuestion(question);
		Assert.assertEquals(question, poolDetailController.getQuestion());
	}
	
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void poolTest() {
		PoolDetailController poolDetailController = new PoolDetailController();
		Pool pool = new Pool();
		pool.setTopic("Tester");
		poolDetailController.setPool(pool);
		Assert.assertEquals(pool, poolDetailController.getPool());
	}
	
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void currentPoolTest() {
		PoolDetailController poolDetailController = new PoolDetailController();
		Pool pool = new Pool();
		pool.setTopic("Tester");
		poolDetailController.setCurrentPool(pool);
		Assert.assertEquals(pool, poolDetailController.getCurrentPool());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void amountTest() {
		PoolDetailController poolDetailController = new PoolDetailController();
		poolDetailController.setAmount(5);
		Assert.assertEquals(5, poolDetailController.getAmount());
	}
	
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void questionDeleteTest() {
		PoolDetailController poolDetailController = new PoolDetailController();
		Question question = new Question();
		question.setQuestion("Tester");
		poolDetailController.setQuestionDelete(question);
		Assert.assertEquals(question, poolDetailController.getQuestionDelete());
	}
	
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void answersTest() {
		PoolDetailController poolDetailController = new PoolDetailController();
		Answer answer = new Answer();
		answer.setAnswer("Tester");
		List<Answer> answers= new ArrayList<>();
		answers.add(answer);
		poolDetailController.setAnswers(answers);
		Assert.assertEquals(answers, poolDetailController.getAnswers());
	}
	
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void showDreiTest() {
		PoolDetailController poolDetailController = new PoolDetailController();
		poolDetailController.setAmount(5);
		Assert.assertEquals(true, poolDetailController.showDrei());
	}
	
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void showVierTest() {
		PoolDetailController poolDetailController = new PoolDetailController();
		poolDetailController.setAmount(5);
		Assert.assertEquals(true, poolDetailController.showVier());
	}
	
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void showFunfTest() {
		PoolDetailController poolDetailController = new PoolDetailController();
		poolDetailController.setAmount(5);
		Assert.assertEquals(true, poolDetailController.showFunf());
	}
	
	
}