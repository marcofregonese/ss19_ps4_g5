package at.qe.sepm.skeleton.ui.beans;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.GameMode;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.ui.beans.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class SecurityTestBeanTest {


	@Test
	public void showOkDialogTest() {
		SecurityTestBean securityTestBean = new SecurityTestBean();
		Assert.assertEquals(false, securityTestBean.isShowOkDialog());
	}

	@Test
	public void performedActionTest() {
		SecurityTestBean securityTestBean = new SecurityTestBean();
		Assert.assertEquals("NONE", securityTestBean.getPerformedAction());
	}
	
	@Test
	public void adminActionTest() {
		SecurityTestBean securityTestBean = new SecurityTestBean();
		securityTestBean.doAdminAction();
		Assert.assertEquals("ADMIN", securityTestBean.getPerformedAction());
		Assert.assertEquals(true, securityTestBean.isShowOkDialog());
	}
	
	@Test
	public void teacherActionTest() {
		SecurityTestBean securityTestBean = new SecurityTestBean();
		securityTestBean.doTeacherAction();
		Assert.assertEquals("TEACHER", securityTestBean.getPerformedAction());
		Assert.assertEquals(true, securityTestBean.isShowOkDialog());
	}
	
	@Test
	public void userActionTest() {
		SecurityTestBean securityTestBean = new SecurityTestBean();
		securityTestBean.doUserAction();
		Assert.assertEquals("USER", securityTestBean.getPerformedAction());
		Assert.assertEquals(true, securityTestBean.isShowOkDialog());
	}
	@Test
	public void hideOkDialogTest() {
		SecurityTestBean securityTestBean = new SecurityTestBean();
		securityTestBean.doHideOkDialog();
		Assert.assertEquals(false, securityTestBean.isShowOkDialog());
	}
}