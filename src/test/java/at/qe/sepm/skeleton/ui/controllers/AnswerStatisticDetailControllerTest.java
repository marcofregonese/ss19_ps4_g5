package at.qe.sepm.skeleton.ui.controllers;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.GameMode;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.AnswerStatisticService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AnswerStatisticDetailControllerTest {

	@Autowired
	private UserService userService;
	@Autowired
	private AnswerStatisticService answerStatisticService;
	private AnswerStatisticDetailController a;

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void userTest() {
		a = new AnswerStatisticDetailController();
		User user = new User();
		userService.saveUser(user);
		user.setUsername("Tester");
		userService.saveUser(user);

		a.setUser(user);

		Assert.assertEquals(a.getUser(), user);  
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void reloadUserTest() {
		a = new AnswerStatisticDetailController();
		User user = new User();
		user.setUsername("Tester");
		a.setUser(user);
		userService.saveUser(user);
		a.doReloadUser();
		Assert.assertEquals(a.getUser(), user);  
	}



}