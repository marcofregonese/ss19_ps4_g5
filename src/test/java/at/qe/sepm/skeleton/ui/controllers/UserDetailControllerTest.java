package at.qe.sepm.skeleton.ui.controllers;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.GameMode;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.AnswerStatisticService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserDetailControllerTest {

	//@Autowired
	//PoolDetailController poolDetailController;

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void question123Test() {
		UserDetailController userDetailController = new UserDetailController();
		User user = new User();
		userDetailController.setNewUser(user);
		Assert.assertEquals(user, 		userDetailController.getNewUser());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void userRoleTest() {
		UserDetailController userDetailController = new UserDetailController();
		UserRole userRole = UserRole.ADMIN;
		userDetailController.setChooseRole(userRole);
		Assert.assertEquals( userDetailController.getChooseRole(), userDetailController.getChooseRole());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void userTest() {
		UserDetailController userDetailController = new UserDetailController();
		User user = new User();
		userDetailController.setUser(user);
		Assert.assertEquals(user, userDetailController.getUser());
	}


}