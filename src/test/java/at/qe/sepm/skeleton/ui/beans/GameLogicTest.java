package at.qe.sepm.skeleton.ui.beans;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.GameMode;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.ui.beans.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GameLogicTest {


	@Test
	public void gamesTest() {
		GameLogic gameLogic = new GameLogic();
		Map<Integer,Game> games=new HashMap<Integer,Game>();
		Game game = new Game();
		games.put(1, game);
		gameLogic.addGame(1, game);
		Assert.assertEquals(games.get(1), gameLogic.getGames().get(1));
	}
	
	@Test
	public void addGameTest() {
		GameLogic gameLogic = new GameLogic();
		Map<Integer,Game> games=new HashMap<Integer,Game>();
		Integer inte = 12;
		Game game = new Game();
		games.put(inte, game);
		gameLogic.addGame(inte,game);
		Assert.assertEquals(games, gameLogic.getGames());
	}
	
	@Test
	public void updateGameTest() {
		GameLogic gameLogic = new GameLogic();
		Map<Integer,Game> games=new HashMap<Integer,Game>();
		Integer inte = 12;
		Game game = new Game();
		games.put(inte, game);
		gameLogic.addGame(inte,game);
		Assert.assertEquals(games, gameLogic.getGames());
	}

}