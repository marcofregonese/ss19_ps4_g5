package at.qe.sepm.skeleton.ui.controllers;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.GameMode;
import at.qe.sepm.skeleton.model.GameRoom;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.AnswerStatisticService;
import at.qe.sepm.skeleton.services.GameRoomService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GameRoomDetailControllerTest {

	//@Autowired
	//PoolDetailController poolDetailController;
	@Autowired
	private GameRoomService gameRoomService;
	
	
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void gameModeTest() {
		GameRoomDetailController gameRoomDetailController = new GameRoomDetailController();
		int var = 1;
		gameRoomDetailController.setGameMode(var);
		Assert.assertEquals(var, gameRoomDetailController.getGameMode());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void selectedHurnTest() {
		GameRoomDetailController gameRoomDetailController = new GameRoomDetailController();
		gameRoomDetailController.setSelectedHurn("Tester");
		Assert.assertEquals("Tester", gameRoomDetailController.getSelectedHurn());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void gameRoomTest() {
		GameRoomDetailController gameRoomDetailController = new GameRoomDetailController();
		GameRoom gameRoom = new GameRoom();
		gameRoomDetailController.setGameRoom(gameRoom);
		Assert.assertEquals(gameRoom, gameRoomDetailController.getGameRoom());
	}


	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void poolListTest() {
		GameRoomDetailController gameRoomDetailController = new GameRoomDetailController();	
		GameRoom gameRoom = new GameRoom();
		gameRoomDetailController.setGameRoom(gameRoom);
		Pool pool = new Pool();
		List<Pool> pools= new ArrayList<>();
		pools.add(pool);
		gameRoomDetailController.setPoolList(pools);
		Assert.assertEquals(pools, 	gameRoomDetailController.getPoolList());

	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void gridTest() {
		GameRoomDetailController gameRoomDetailController = new GameRoomDetailController();
		long var = 1;
		gameRoomDetailController.setGrid(var);
		Assert.assertEquals(var, gameRoomDetailController.getGrid());
	}
	
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void selectedPoolsTest() {
		GameRoomDetailController gameRoomDetailController = new GameRoomDetailController();
		gameRoomDetailController.setSelectedPools("Tester");
		Assert.assertEquals("Tester", gameRoomDetailController.getSelectedPools());
	}
	
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void numberAnswerTest() {
		GameRoomDetailController gameRoomDetailController = new GameRoomDetailController();
		gameRoomDetailController.setNumberAnswers("Tester");
		Assert.assertEquals("Tester", gameRoomDetailController.getNumberAnswers());
	}
	
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void maxPlayerTest() {
		GameRoomDetailController gameRoomDetailController = new GameRoomDetailController();
		int var = 4;
		gameRoomDetailController.setMaxPlayers(var);
		Assert.assertEquals(var, gameRoomDetailController.getMaxPlayers());
	}
}