package at.qe.sepm.skeleton.ui.controllers;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.GameMode;
import at.qe.sepm.skeleton.model.GameRoom;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.AnswerStatisticService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GameControllerTest {

	//@Autowired
	//PoolDetailController poolDetailController;

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void question123Test() {
		GameController gameController = new GameController();
		int var = 12;
		gameController.setVar(var);
		Assert.assertEquals(var, gameController.getVar());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void answersTest() {
		GameController gameController = new GameController();
		Answer answer = new Answer();
		answer.setAnswer("Tester");
		List<Answer> answers= new ArrayList<>();
		answers.add(answer);
		gameController.setAnswers(answers);
		Assert.assertEquals(answers, gameController.getAnswers());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void gameIdTest() {
		GameController gameController = new GameController();
		int var = 12;
		gameController.setGameId(var);
		Assert.assertEquals(var, gameController.getGameId());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void gameTest() {
		GameController gameController = new GameController();
		Game game = new Game();
		gameController.setGame(game);
		Assert.assertEquals(game, gameController.getGame());
	}
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void userTest() {
		GameController gameController = new GameController();
		User user = new User();
		gameController.user(user);
		Assert.assertEquals(user, gameController.getUser());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void questionTest() {
		GameController gameController = new GameController();
		Question question = new Question();
		gameController.setQuestion(question);
		Assert.assertEquals(question, gameController.getQuestion());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void gameoverTest() {
		GameController gameController = new GameController();
		boolean gameover = true;
		gameController.setGameover(gameover);
		Assert.assertEquals(gameover, gameController.isGameover());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void answerTest() {
		GameController gameController = new GameController();
		Answer answer = new Answer();
		gameController.setAnswer(answer);
		Assert.assertEquals(answer, gameController.getAnswer());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void questionsTest() {
		GameController gameController = new GameController();
		Question question = new Question();
		question.setQuestion("Tester");
		List<Question> questions= new ArrayList<>();
		questions.add(question);
		gameController.setQuestions(questions);
		Assert.assertEquals(questions, gameController.getQuestions());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void leaveGameTest() {
		GameController gameController = new GameController();
		Assert.assertEquals("secured/welcome.xhtml?faces-redirect=true", gameController.leaveGame());
	}


	//	@Test
	//	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	//	public void stop2Test() {
	//		GameController gameController = new GameController();
	//		Integer progress = 0;
	//		gameController.setProgress(progress);
	//		Assert.assertEquals(false, gameController.stop());
	//	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void jokerAvailableTest() {
		GameController gameController = new GameController();
		Game game = new Game();
		game.setJokerUsed(false);
		gameController.setGame(game);
		Assert.assertEquals(true, gameController.jokerAvailable());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void statsCurrentPoolTest(){
		GameController gameController = new GameController();
		Assert.assertEquals(null, gameController.getStatsCurrentPool());
	}
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void questionModeTest(){
		GameController gameController = new GameController();
		Game game = new Game();
		GameMode gameMode = GameMode.QUESTION;
		game.setGameMode(gameMode);
		gameController.setGame(game);
		Assert.assertEquals(true, gameController.questionMode());
	}
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void questionMode2Test(){
		GameController gameController = new GameController();
		Game game = new Game();
		GameMode gameMode = GameMode.ANSWER;
		game.setGameMode(gameMode);
		gameController.setGame(game);
		Assert.assertEquals(false, gameController.questionMode());
	}
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void answerModeTest(){
		GameController gameController = new GameController();
		Game game = new Game();
		GameMode gameMode = GameMode.ANSWER;
		game.setGameMode(gameMode);
		gameController.setGame(game);
		Assert.assertEquals(true, gameController.answerMode());
	}
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void answerMode2Test(){
		GameController gameController = new GameController();
		Game game = new Game();
		GameMode gameMode = GameMode.QUESTION;
		game.setGameMode(gameMode);
		gameController.setGame(game);
		Assert.assertEquals(false, gameController.answerMode());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void progressTest(){
		GameController gameController = new GameController();
		Integer progress = 12;
		gameController.setProgress(12);
		Assert.assertEquals(progress, gameController.getProgress());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void decrementTest(){
		GameController gameController = new GameController();
		Integer progress = 12;
		gameController.setProgress(12);
		progress=progress-1;
		gameController.decrement();
		Assert.assertEquals(progress, gameController.getProgress());
	}
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void cancelTest(){
		GameController gameController = new GameController();
		Integer progress = 12;
		gameController.setProgress(progress);
		gameController.cancel();
		Assert.assertEquals(gameController.getProgress(), gameController.getProgress());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void answerRevTest() {
		GameController gameController = new GameController();
		Answer answer = new Answer();
		answer.setAnswer("Tester");
		List<Answer> answers= new ArrayList<>();
		answers.add(answer);
		Question question = new Question();
		question.setAnswers(answers);
		gameController.setQuestion(question);
		gameController.setAnswers(answers);
		Assert.assertEquals("Tester", gameController.answerRev());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void gameRoomTest(){
		GameController gameController = new GameController();
		GameRoom gameRoom = new GameRoom();
		gameController.setGameroom(gameRoom);
		Assert.assertEquals(gameRoom, gameController.getGameroom());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void gameLogicTest(){
		GameController gameController = new GameController();
		GameLogic gameLogic = new GameLogic();
		gameController.setGameLogic(gameLogic);
		Assert.assertEquals(gameLogic, gameController.getGameLogic());
	}
	

}