package at.qe.sepm.skeleton.tests;
import static org.junit.Assert.*;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.*;
import org.springframework.security.test.context.support.WithMockUser;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;


import static junit.framework.TestCase.assertTrue;


public class GruppeVTest {
	
	
    @Test
    public void loginTest(){

        WebDriver browser = new ChromeDriver();
        browser.get("http://localhost:8080");
        WebElement form = browser.findElement(By.id("login_form"));
        assertTrue((form.isDisplayed()));

        WebElement username = browser.findElement((By.id("username")));
        WebElement password = browser.findElement((By.id("password")));

        username.sendKeys("admin");
        password.sendKeys("passwd");
        form.submit();

        browser.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        assertTrue("login failed", browser.getCurrentUrl().endsWith("welcome.xhtml"));



        browser.close();
    }
    
    @Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
    public void createQuestionpoolTest(){

    	 WebDriver browser = new ChromeDriver();
         browser.get("http://localhost:8080");
         WebElement form = browser.findElement(By.id("login_form"));
         assertTrue((form.isDisplayed()));

         WebElement username = browser.findElement((By.id("username")));
         WebElement password = browser.findElement((By.id("password")));

         username.sendKeys("admin");
         password.sendKeys("passwd");
         form.submit();
    	
        browser.get("http://localhost:8080/teacher/createpool.xhtml");
        form = browser.findElement(By.id("form_lol"));
        assertTrue((form.isDisplayed()));

//        WebElement topic = browser.findElement((By.id("topic")));
//        Select difficulty = new Select(browser.findElement((By.id("difficulty"))));
//        Select amount = new Select(browser.findElement((By.id("amount"))));
        //WebElement ok = browser.findElement(By.id("ok"));
        
        browser.findElement(By.xpath("//input[@value='Add questions' and @type='button']")).click();


//        topic.sendKeys("hallo");
//        difficulty.selectByVisibleText("EASY");
//        amount.selectByVisibleText("4");
        //ok.click();
//        form.submit();

        
        browser.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        assertTrue(form.isDisplayed());

        browser.close();
    }
	
	
	
    @Test
   	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
       public void createUserTest(){
    	
    	WebDriver browser = new ChromeDriver();
        browser.get("http://localhost:8080");
        WebElement form = browser.findElement(By.id("login_form"));
        assertTrue((form.isDisplayed()));

        WebElement username = browser.findElement((By.id("username")));
        WebElement password = browser.findElement((By.id("password")));

        username.sendKeys("admin");
        password.sendKeys("passwd");
        form.submit();
   	
       browser.get("http://localhost:8080/teacher/createUser.xhtml");
       form = browser.findElement(By.id("userForm2"));
     //  assertTrue((form.isDisplayed()));
       
       WebElement username1 = browser.findElement((By.id("userForm2:username")));
       WebElement password1 = browser.findElement((By.id("userForm2:password")));
 
       WebElement firstname = browser.findElement((By.id("userForm2:firstname")));
       WebElement lastname = browser.findElement((By.id("userForm2:lastname")));
       WebElement email = browser.findElement((By.id("userForm2:email")));
       //WebElement button = browser.findElement((By.name("userForm2:h")));
       
       
       
       username1.sendKeys("alex");
       password1.sendKeys("passwd");
       firstname.sendKeys("alex");
       lastname.sendKeys("uolki");
       email.sendKeys("emaileer");
     //  button.click();
       browser.findElement(By.name("h")).sendKeys(Keys.ENTER);
       form.submit();
       
       assertTrue(true);

    }
    
    
//    @Test
//	public void site_header_is_on_home_page() {   
//		System.setProperty("webdriver.chrome.driver","chromedriver.exe");   
//		WebDriver browser = new ChromeDriver();
//		browser.get("https://www.saucelabs.com");       
//		WebElement href = browser.findElement(By.xpath("//a[@href='/beta/login']"));
//		assertTrue((href.isDisplayed()));  
//		browser.close();
//
//	}

//	@Test
//	public void test() {
//		fail("Not yet implemented");
//	}

}
