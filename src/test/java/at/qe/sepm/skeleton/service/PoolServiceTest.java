package at.qe.sepm.skeleton.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class PoolServiceTest {

	@Autowired
	private GameRoomService gameroomservice;
	@Autowired
	private QuestionService questionservice;
	@Autowired
	private PoolService poolservice;
	@Autowired
	private UserService userservice;

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getQuestionTest() {
		Pool pool = new Pool();
		pool.setId(1L);
		pool.setTopic("Tester");
		poolservice.savePool(pool);
		
		Pool ret = poolservice.getByID(1L);
		
		Assert.assertEquals(ret.getTopic(), pool.getTopic());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getByTopicTest() {
		Pool pool = new Pool();
		pool.setId(1L);
		pool.setTopic("Tester");
		poolservice.savePool(pool);
		
		Collection<Pool> ret = poolservice.getByTopic("Tester");
		
		Assert.assertEquals(ret.size(), 1);
	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getByStartTimeBetweenTest() {
		LocalTime startTime = LocalTime.now();
		Pool pool = new Pool();
		pool.setId(1L);
		pool.setStartTime(startTime);
		poolservice.savePool(pool);
		Collection<Pool> ret = poolservice.getByStartTimeBetween(LocalTime.now().minusHours(2), LocalTime.now().plusHours(2));
		Assert.assertEquals(ret.size(), 1);
	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getByEndTimeBetweenTest() {
		LocalTime startTime = LocalTime.now();
		Pool pool = new Pool();
		pool.setId(1L);
		pool.setEndTime(startTime);
		poolservice.savePool(pool);
		Collection<Pool> ret = poolservice.getByEndTimeBetween(LocalTime.now().minusHours(2), LocalTime.now().plusHours(2));
		Assert.assertEquals(ret.size(), 1);
	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getByTotalPointsBetweenTest() {
		Pool pool = new Pool();
		pool.setId(1L);
		pool.setTotalPoints(100);
		poolservice.savePool(pool);
		Collection<Pool> ret = poolservice.getByTotalPointsBetween(50, 150);
		Assert.assertEquals(ret.size(), 1);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getAllPoolsTest() {
		Pool pool = new Pool();
		poolservice.savePool(pool);
		Pool pool2 = new Pool();
		poolservice.savePool(pool2);
		Pool pool3 = new Pool();
		poolservice.savePool(pool3);
		
		Collection<Pool> ret = poolservice.getAllPools();
		Assert.assertEquals(ret.size(), 3);
	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getDeletePoolTest() {
		Pool pool = new Pool();
		pool.setId(1L);
		poolservice.savePool(pool);
		
		Pool ret = poolservice.getByID(1);
		Assert.assertEquals(ret.getId(), pool.getId());
		
		poolservice.deletePool(pool);
		ret = poolservice.getByID(1);
		Assert.assertNull(ret);

	}
	
}