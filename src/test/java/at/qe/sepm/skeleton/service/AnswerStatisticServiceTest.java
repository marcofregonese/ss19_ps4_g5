package at.qe.sepm.skeleton.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class AnswerStatisticServiceTest {

	@Autowired
	private AnswerService answerService;
	@Autowired
	private AnswerStatisticService answerStatisticService;
	@Autowired
	private UserService userService;
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getStatisticByIdTest() {
		long id = 1;
		AnswerStatistic answer1 = new AnswerStatistic();
		answer1.setId(id);
		answerStatisticService.saveAnswerStatistic(answer1);
		AnswerStatistic ret = answerStatisticService.getStatisticById(id);
		Assert.assertEquals(answer1.getId(), ret.getId());
	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getStatisticByPlayerTest() {
		User p = new User();
		p.setUsername("uolki");
		p.setFirstName("HURN");
		userService.saveUser(p);	
		List<AnswerStatistic> as = new ArrayList();
		AnswerStatistic answer1 = new AnswerStatistic();
		answer1.setId(1L);
		answer1.setPlayer(p);	
		as.add(answer1);	
		answerStatisticService.saveAnswerStatistic(answer1);
		AnswerStatistic ret = answerStatisticService.getStatisticByPlayer(p);
		System.out.println(ret);
		Assert.assertEquals(answer1.getId(), ret.getId());
	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findAllTest() {
		AnswerStatistic answer1 = new AnswerStatistic();
		AnswerStatistic answer2 = new AnswerStatistic();
		AnswerStatistic answer3 = new AnswerStatistic();
		answer1.setId(1L);
		answer2.setId(2L);
		answer3.setId(3L);
		answerStatisticService.saveAnswerStatistic(answer1);
		answerStatisticService.saveAnswerStatistic(answer2);
		answerStatisticService.saveAnswerStatistic(answer3);
		Collection<AnswerStatistic> ret  = answerStatisticService.findAll();
		Assert.assertEquals(3, ret.size());
	}

//	@DirtiesContext
//	@Test
//	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
//	public void getAnswerStatisticsTest() {
//		AnswerStatistic answer1 = new AnswerStatistic();
//		AnswerStatistic answer2 = new AnswerStatistic();
//		AnswerStatistic answer3 = new AnswerStatistic();
//		User p = new User();
//		p.setUsername("uolki");
//		p.setFirstName("HURN");
//		userService.saveUser(p);
//		
//		answer1.setId(8L);
//		answer1.setPlayer(p);
//		answer2.setId(9L);
//		answer2.setPlayer(p);
//		answer3.setId(10L);
//		answer3.setPlayer(p);
//
//		
//		
//		answerStatisticService.saveAnswerStatistic(answer1);
//		answerStatisticService.saveAnswerStatistic(answer2);
//		answerStatisticService.saveAnswerStatistic(answer3);
//
//		List<AnswerStatistic> h =  new ArrayList<>();
//		h.add(answer1);
//		h.add(answer2);
//		h.add(answer3);
//		p.setAnswerStatistics(h);
//		userService.saveUser(p);
//		System.out.println(p);
//		List<AnswerStatistic> ret  = answerStatisticService.getAnswerStatistics(p);
//		
//		Assert.assertEquals(3, ret.size());
//	}
	
}