package at.qe.sepm.skeleton.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class GameRoomServcieTest {

	@Autowired
	private GameRoomService gameroomservice;
	@Autowired
	private QuestionService questionservice;
	@Autowired
	private PoolService poolservice;
	@Autowired
	private UserService userservice;

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getQuestionTest() {
		GameRoom gameroom = new GameRoom();
		gameroomservice.saveGameRoom(gameroom);

		Pool pool = new Pool();
		poolservice.savePool(pool);

		Question question = new Question();	
		questionservice.saveQuestion(question);		

		question.setQuestion("Hallo?");
		List<Pool> polllist =  new ArrayList<>();
		polllist.add(pool);
		List<Question> questionlist = new ArrayList<>();
		questionlist.add(question);
		pool.setQuestions(questionlist);
		gameroom.setPools(polllist);


		List<Question> ret = gameroomservice.getQuestion(pool);
		Assert.assertEquals(ret.size(), questionlist.size());
	}


	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getByStartDateBetweenTest() {
		GameRoom gameroom = new GameRoom();
		LocalDate startDate = LocalDate.now();
		gameroom.setStartDate(startDate);
		gameroomservice.saveGameRoom(gameroom);
		//Collection<GameRoom> gamerooms = new ArrayList<>();
		//gamerooms.add(gameroom);
		Collection<GameRoom> ret = gameroomservice.getByStartDateBetween(LocalDate.now().minusDays(1), LocalDate.now().plusDays(1));

		Assert.assertEquals(ret.size(), 1);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getByEndDateBetweenTest() {
		GameRoom gameroom = new GameRoom();
		LocalDate endDate = LocalDate.now();
		gameroom.setEndDate(endDate);
		gameroomservice.saveGameRoom(gameroom);

		Collection<GameRoom> ret = gameroomservice.getByEndDateBetween(LocalDate.now().minusDays(1), LocalDate.now().plusDays(1));

		Assert.assertEquals(ret.size(), 1);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getByStartTimeBetweenTest() {
		GameRoom gameroom = new GameRoom();
		LocalTime startTime = LocalTime.now();
		gameroom.setStartTime(startTime);
		gameroom.setTeamName("hallo");
		gameroomservice.saveGameRoom(gameroom);
		Collection<GameRoom> ret = gameroomservice.getByStartTimeBetween(LocalTime.now().minusHours(12), LocalTime.now().plusHours(12));

		Assert.assertEquals(ret.size(), 1);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getByEndTimeBetweenTest() {
		GameRoom gameroom = new GameRoom();
		LocalTime endTime = LocalTime.now();
		gameroom.setEndTime(endTime);
		gameroomservice.saveGameRoom(gameroom);
		Collection<GameRoom> ret = gameroomservice.getByEndTimeBetween(LocalTime.now().minusHours(12), LocalTime.now().plusHours(12));

		Assert.assertEquals(ret.size(), 1);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getByUserTest() {
		GameRoom gameroom = new GameRoom();
		gameroomservice.saveGameRoom(gameroom);
		gameroom.setTeamName("Tester");
		gameroomservice.saveGameRoom(gameroom);
		User user = new User();
		userservice.saveUser(user);
		user.setGameRooms(gameroom);		
		userservice.saveUser(user);

		GameRoom ret = gameroomservice.getByUser(user);
		
		Assert.assertEquals(ret.getTeamName(), user.getGameRooms().getTeamName());

	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getByIdTest() {
		GameRoom gameroom = new GameRoom();
		gameroom.setId(1L);
		gameroomservice.saveGameRoom(gameroom);
		
		GameRoom ret = gameroomservice.getById(1);

		System.out.println(gameroom.getId());
		Assert.assertEquals(ret.getId(), gameroom.getId());

	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getDeleteGameRoomTest() {
		GameRoom gameroom = new GameRoom();
		gameroom.setId(1L);
		gameroomservice.saveGameRoom(gameroom);
		
		GameRoom ret = gameroomservice.getById(1);
		Assert.assertEquals(ret.getId(), gameroom.getId());
		
		gameroomservice.deleteGameRoom(gameroom);
		ret = gameroomservice.getById(1);
		Assert.assertNull(ret);

	}
	


}