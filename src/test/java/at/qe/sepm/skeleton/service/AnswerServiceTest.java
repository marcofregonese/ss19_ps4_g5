package at.qe.sepm.skeleton.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Collection;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class AnswerServiceTest {

	@Autowired
	private AnswerService answerService;

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findByIdTest() {
		long id = 1;
		Answer answer1 = new Answer();
		answer1.setId(id);
		answerService.saveAnswer(answer1);
		Answer ret = answerService.findById(id);
		Assert.assertEquals(answer1.getId(), ret.getId());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findAllTest() {
		Answer answer1 = new Answer();
		Answer answer2 = new Answer();
		Answer answer3 = new Answer();
		answer1.setId(1L);
		answer2.setId(2L);
		answer3.setId(3L);
		answerService.saveAnswer(answer1);
		answerService.saveAnswer(answer2);
		answerService.saveAnswer(answer3);
		Collection<Answer> ret  = answerService.findAll();
		Assert.assertEquals(3, ret.size());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findByAnswerContainingTest() {
		String h = "text";
		Answer answer1 = new Answer();
		Answer answer2 = new Answer();
		answer1.setAnswer(h);
		answer2.setAnswer(h);
		answerService.saveAnswer(answer1);
		answerService.saveAnswer(answer2);
		Collection<Answer> ret = answerService.findByAnswerContaining(h);
		Assert.assertEquals(2, ret.size());
	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findByTopicTest() {
		String h = "Computerspiele easy";
		Answer answer1 = new Answer();
		Answer answer2 = new Answer();
		answer1.setTopic(h);
		answer2.setTopic(h);
		answerService.saveAnswer(answer1);
		answerService.saveAnswer(answer2);
		Collection<Answer> ret = answerService.findByTopic(h);
		Assert.assertEquals(2, ret.size());
	}

	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findByDifficultyTest() {
		String h = "EASY";
		Answer answer1 = new Answer();
		Answer answer2 = new Answer();
		answer1.setDifficulty(h);
		answer2.setDifficulty(h);
		answerService.saveAnswer(answer1);
		answerService.saveAnswer(answer2);
		Collection<Answer> ret = answerService.findByDifficulty(h);
		Assert.assertEquals(2, ret.size());
	}

	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findByPoolTagTest() {
		String h = "Coputerspiele easy";
		Pool p = new Pool();
		p.setTopic(h);
		Answer answer1 = new Answer();
		Answer answer2 = new Answer();
		answer1.setPool(p);
		answer2.setPool(p);
		answerService.saveAnswer(answer1);
		answerService.saveAnswer(answer2);
		Collection<Answer> ret = answerService.findByPoolTag(p.getTopic());
		Assert.assertEquals(2, ret.size());
	}
}