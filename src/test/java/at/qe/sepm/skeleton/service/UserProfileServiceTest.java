package at.qe.sepm.skeleton.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class UserProfileServiceTest {

	@Autowired
	private QuestionService questionService;
	@Autowired
	private UserProfileService userprofileservice;
	@Autowired
	private AnswerStatisticService answerstatisticservice;

//	@DirtiesContext
//	@Test
//	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
//	public void findAllTest() {
//		AnswerStatistic answerstatistic = new AnswerStatistic();
//		answerstatisticservice.saveAnswerStatistic(answerstatistic);
//		AnswerStatistic answerstatistic2 = new AnswerStatistic();
//		answerstatisticservice.saveAnswerStatistic(answerstatistic2);
//		
//		Collection<AnswerStatistic> ret = userprofileservice.findAll();
//		
//		Assert.assertEquals(ret.size(), 2);
//	}
	
}