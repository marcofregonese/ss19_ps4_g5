package at.qe.sepm.skeleton.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class QuestionServiceTest {

	@Autowired
	private QuestionService questionService;
	@Autowired
	private PoolService poolservice;
	@Autowired
	private AnswerService answerservice;

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findByIdTest() {
		long id = 1;
		Question question = new Question();
		question.setId(id);
		questionService.saveQuestion(question);
		
		Question ret = questionService.findById(id);
		
		Assert.assertEquals(ret.getId(), question.getId());
	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findAllTest() {
		Question question = new Question();
		questionService.saveQuestion(question);
		Question question2 = new Question();
		questionService.saveQuestion(question2);
		Question question3 = new Question();
		questionService.saveQuestion(question3);
		
		Collection<Question> ret = questionService.findAll();
		
		Assert.assertEquals(ret.size(), 3);

	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findByQuestionContainingTest() {
		Question question = new Question();
		question.setQuestion("Tester");
		questionService.saveQuestion(question);
		Question question2 = new Question();
		question2.setQuestion("Tester");
		questionService.saveQuestion(question2);
		
		Collection<Question> ret = questionService.findByQuestionContaining("Tester");

		Assert.assertEquals(ret.size(), 2);

	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findByTopicTest() {
		Question question = new Question();
		question.setTopic("Tester");
		questionService.saveQuestion(question);
		Question question2 = new Question();
		question2.setTopic("Tester");
		questionService.saveQuestion(question2);
		
		Collection<Question> ret = questionService.findByTopic("Tester");

		Assert.assertEquals(ret.size(), 2);

	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findByDifficultyTest() {
		Question question = new Question();
		question.setDifficulty("Tester");
		questionService.saveQuestion(question);
		Question question2 = new Question();
		question2.setDifficulty("Tester");
		questionService.saveQuestion(question2);
		
		Collection<Question> ret = questionService.findByDifficulty("Tester");

		Assert.assertEquals(ret.size(), 2);

	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void findByPoolTagTest() {
		Pool pool = new Pool();
		poolservice.savePool(pool);
		Question question = new Question();
		question.setPool(pool);
		questionService.saveQuestion(question);
		Question question2 = new Question();
		question2.setPool(pool);
		questionService.saveQuestion(question2);
		List<Question> questions = new ArrayList<>();
		questions.add(question);
		questions.add(question2);
		pool.setQuestions(questions);
		poolservice.savePool(pool);
	
		
		Collection<Question> ret = questionService.findByPoolTag(pool);

		
		Assert.assertEquals(ret.size(), 2);

	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getRandomQuestionTest() {
		Question question = new Question();
		questionService.saveQuestion(question);
		Question question2 = new Question();
		questionService.saveQuestion(question2);
		Collection<Question> ret = questionService.getRandomQuestion();		
		Assert.assertEquals(ret.size(), 2);

	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getRandomAnswerTest() {
		Answer answer = new Answer();
		answerservice.saveAnswer(answer);
		Answer answer2 = new Answer();
		answerservice.saveAnswer(answer2);

		Collection<Answer> ret = questionService.getRandomAnswer();
		
		Assert.assertEquals(ret.size(), 2);

	}
	
}