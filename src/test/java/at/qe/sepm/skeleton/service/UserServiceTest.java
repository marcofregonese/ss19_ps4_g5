package at.qe.sepm.skeleton.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class UserServiceTest {

	@Autowired
	private GameRoomService gameroomservice;
	@Autowired
	private QuestionService questionservice;
	@Autowired
	private PoolService poolservice;
	@Autowired
	private UserService userservice;

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void getAllUsersTest() {
		User user = new User();
		userservice.saveUser(user);
		User user2 = new User();
		userservice.saveUser(user2);

		Collection<User> ret = userservice.getAllUsers();
		System.out.println(ret.size());
		
		Assert.assertEquals(ret.size(), 2);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void loadUserTest() {
		User user = new User();
		user.setUsername("Tester");
		userservice.saveUser(user);
		
		User ret = userservice.loadUser("Tester");
		Assert.assertEquals(ret.getUsername(), "Tester");

	}
	
	@DirtiesContext
	@Test
	@WithMockUser(username = "admin", authorities = {"ADMIN", "TEACHER"})
	public void deleteUserTest() {
		User user = new User();
		user.setUsername("Tester");
		userservice.saveUser(user);
		
		User ret = userservice.loadUser("Tester");
		Assert.assertEquals(ret.getUsername(), "Tester");
		
		userservice.deleteUser(user);
		ret = userservice.loadUser("Tester");
		Assert.assertNull(ret);

	}
	
}