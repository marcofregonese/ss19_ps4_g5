package at.qe.sepm.skeleton.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.util.List;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AnswerStatisticTest {

    @Test
    public void setIdTest() {
        AnswerStatistic answer = new AnswerStatistic();
        Long a = 0L;
        answer.setId(a);
        Assert.assertEquals(a, answer.getId());
    }
    

    @Test
    public void setDifficultyTest() {
    	AnswerStatistic answer = new AnswerStatistic();        
    	String a = "tester";
        answer.setDifficulty(a);
        Assert.assertEquals(a, answer.getDifficulty());
    }
    
    @Test
    public void setTopicTest() {
    	AnswerStatistic answer = new AnswerStatistic();
    	String a = "tester";
        answer.setTopic(a);
        Assert.assertEquals(a, answer.getTopic());
    }
    
    @Test
    public void setNoSucceedTest() {
    	AnswerStatistic answer = new AnswerStatistic();
    	Integer a = 0;
        answer.setNoSucceed(a);
        Assert.assertEquals(a, answer.getNoSucceed());
    }
    
    @Test
    public void setNoFailedTest() {
    	AnswerStatistic answer = new AnswerStatistic();
    	Integer a = 0;
        answer.setNoFailed(a);
        Assert.assertEquals(a, answer.getNoFailed());
    }
    
    @Test
    public void setPercentageTest() {
    	AnswerStatistic answer = new AnswerStatistic();
    	Double a = 0.0;
        answer.setPercentage(a);
        Assert.assertEquals(a, answer.getPercentage());
    }
    
    @Test
    public void setPlayerTest() {
    	AnswerStatistic answer = new AnswerStatistic();
    	User a = new User();
        answer.setPlayer(a);
        Assert.assertEquals(a, answer.getPlayer());
    }
    

}