package at.qe.sepm.skeleton.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GameRoomTest {

    
    @Test
    public void setIdTest() {
        GameRoom up = new GameRoom();
        Long a = 0L;
        up.setId(a);
        Assert.assertEquals(a, up.getId());
    }
    
    @Test
    public void serialVersionUIDTest() {
        GameRoom u = new GameRoom();
    	long a = 1L;
        Assert.assertEquals(a, u.getSerialversionuid());
    }
    
    @Test
    public void startDateTest() {
        GameRoom u = new GameRoom();
        LocalDate a = null;
        u.setStartDate(a);
        Assert.assertEquals(a, u.getStartDate());
    }
    
    @Test
    public void endDateTest() {
        GameRoom u = new GameRoom();
        LocalDate a = null;
        u.setEndDate(a);
        Assert.assertEquals(a, u.getEndDate());
    }
    
    @Test
    public void startTimeTest() {
        GameRoom u = new GameRoom();
        LocalTime a = null;
        u.setStartTime(a);
        Assert.assertEquals(a, u.getStartTime());
    }
    
    @Test
    public void endTimeTest() {
        GameRoom u = new GameRoom();
        LocalTime a = null;
        u.setEndTime(a);
        Assert.assertEquals(a, u.getEndTime());
    }
    
    @Test
    public void teamnameTest() {
        GameRoom u = new GameRoom();
        String a = "tester";
        u.setTeamName(a);
        Assert.assertEquals(a, u.getTeamName());
    }
    
    @Test
    public void noRightSolutionTest() {
        GameRoom u = new GameRoom();
        Integer a = 12345;
        u.setNoRightSolution(a);
        Assert.assertEquals(a, u.getNoRightSolution());
    }
    
//    @Test
//    public void usersTest() {
//        GameRoom u = new GameRoom();
//        List<User> a = null;
//        User m = new User();
//        a.add(m);
//        u.setUsers(a);
//        Assert.assertEquals(a, u.getUsers());
//    }
    
    @Test
    public void poolsTest() {
        GameRoom u = new GameRoom();
        List<Pool> a = null;
        u.setPools(a);
        Assert.assertEquals(a, u.getPools());
    }


}