package at.qe.sepm.skeleton.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.util.List;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class QuestionTest {

    @Test
    public void setIdTest() {
        Question question = new Question();
        Long a = 0L;
        question.setId(a);
        Assert.assertEquals(a, question.getId());
    }
    
    @Test
    public void setQuestionTest() {
    	Question question = new Question();
    	String a = "tester";
        question.setQuestion(a);
        Assert.assertEquals(a, question.getQuestion());
    }

    @Test
    public void setAnswersTest() {
    	Question question = new Question();
        List<Answer> a = null;
        question.setAnswers(a);
        Assert.assertEquals(a, question.getAnswers());
    }
    
    @Test
    public void setDifficultyTest() {
    	Question question = new Question();
        String a = "tester";
        question.setDifficulty(a);
        Assert.assertEquals(a, question.getDifficulty());
    }
    
    @Test
    public void setTopicTest() {
    	Question question = new Question();
        String a = "tester";
        question.setTopic(a);
        Assert.assertEquals(a, question.getTopic());
    }
    
    @Test
    public void setPoolTagTest() {
    	Question question = new Question();
        Pool a = new Pool();
        question.setPool(a);
        Assert.assertEquals(a, question.getPool());
    }
    
    @Test
    public void isNewTest() {
    	Question question = new Question();
        boolean a = false;
        Assert.assertEquals(a, question.isNew());
    }
    
    @Test
    public void serialVersionUIDTest() {
    	Question question = new Question();
    	long a = 1L;
        Assert.assertEquals(a, question.getSerialversionuid());
    }

}