package at.qe.sepm.skeleton.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserProfileTest {

    
    @Test
    public void setIdTest() {
        UserProfile up = new UserProfile();
        Long a = 0L;
        up.setId(a);
        Assert.assertEquals(a, up.getId());
    }
    
    @Test
    public void rightPoolsTest() {
        UserProfile up = new UserProfile();
        int a = 123;
        up.setRightPools(a);
        Assert.assertEquals(a, up.getRightPools());
    }
    
    @Test
    public void totalTimeTest() {
        UserProfile up = new UserProfile();
        Long a = 0L;
        String b = "Days: 0 Hours: 0 Minutes: 0 Seconds: 0";
        up.setTotalTime(a);
        Assert.assertEquals(b, up.getTotalTime());
    }
    
    @Test
    public void noSucceedTest() {
        UserProfile up = new UserProfile();
        int a = 123;
        up.setNoSucceed(a);
        Assert.assertEquals(a, up.getNoSucceed());
    }
    
    @Test
    public void noFailTest() {
        UserProfile up = new UserProfile();
        int a = 123;
        up.setNoFailed(a);
        Assert.assertEquals(a, up.getNoFailed());
    }
    
//    @Test
//    public void playedWithTest() {
//        UserProfile up = new UserProfile();
//        List<User> a = null;
//        up.setPlayedWith(a);
//        Assert.assertEquals(a, up.getPlayedWith());
//    }
//    
    @Test
    public void ownerTest() {
        UserProfile up = new UserProfile();
        User a = new User();
        up.setOwner(a);
        Assert.assertEquals(a, up.getOwner());
    }
    
    
    
    @Test
    public void profileTest() {
        User u = new User();
        UserProfile a = new UserProfile();
        u.setProfile(a);
        Assert.assertEquals(a, u.getProfile());
    }
    
   
    @Test
    public void serialVersionUIDTest() {
        UserProfile u = new UserProfile();
    	long a = 1L;
        Assert.assertEquals(a, u.getSerialversionuid());
    }

}