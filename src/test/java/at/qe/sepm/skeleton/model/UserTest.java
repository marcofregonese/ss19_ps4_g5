package at.qe.sepm.skeleton.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserTest {

    
    @Test
    public void setTopicNameTest() {
        User u = new User();
    	String a = "tester";
        u.setUsername(a);
        Assert.assertEquals(a, u.getUsername());
    }
    
    @Test
    public void createUserTest() {
        User u = new User();
        User a = new User();
        u.setCreateUser(a);
        Assert.assertEquals(a, u.getCreateUser());
    }
    
    @Test
    public void updateUserTest() {
        User u = new User();
        User a = new User();
        u.setUpdateUser(a);
        Assert.assertEquals(a, u.getUpdateUser());
    }
    
    @Test
    public void createDateTest() {
        User u = new User();
        Date a = new Date();
        u.setCreateDate(a);
        Assert.assertEquals(a, u.getCreateDate());
    }
    
    @Test
    public void updateDateTest() {
        User u = new User();
        Date a = new Date();
        u.setUpdateDate(a);
        Assert.assertEquals(a, u.getUpdateDate());
    }
    
    @Test
    public void passwordTest() {
        User u = new User();
        String a = "tester";
        u.setPassword(a);
        Assert.assertEquals(a, u.getPassword());
    }
    
    @Test
    public void firstNameTest() {
        User u = new User();
        String a = "tester";
        u.setFirstName(a);
        Assert.assertEquals(a, u.getFirstName());
    }
    
    @Test
    public void lastNameTest() {
        User u = new User();
        String a = "tester";
        u.setLastName(a);
        Assert.assertEquals(a, u.getLastName());
    }
    
    @Test
    public void emailTest() {
        User u = new User();
        String a = "tester";
        u.setEmail(a);
        Assert.assertEquals(a, u.getEmail());
    }
    
    @Test
    public void phoneTest() {
        User u = new User();
        String a = "tester";
        u.setPhone(a);
        Assert.assertEquals(a, u.getPhone());
    }
    
    @Test
    public void enabledTest() {
        User u = new User();
        boolean a = true;
        u.setEnabled(a);
        Assert.assertEquals(a, u.isEnabled());
    }
   
    @Test
    public void rolesTest() {
        User u = new User();
        Set<UserRole> a = null;
        u.setRoles(a);
        Assert.assertEquals(a, u.getRoles());
    }
    
    @Test
    public void gameRoomsTest() {
        User u = new User();
        GameRoom a = new GameRoom();
        u.setGameRooms(a);
        Assert.assertEquals(a, u.getGameRooms());
    }
    
    @Test
    public void poolTest() {
        User u = new User();
        List<Pool> a = null;
        u.setPool(a);
        Assert.assertEquals(a, u.getPool());
    }
    
    @Test
    public void answerStatisticTest() {
        User u = new User();
        List<AnswerStatistic> a = null;
        u.setAnswerStatistics(a);
        Assert.assertEquals(a, u.getAnswerStatistics());
    }
    
//    @Test
//    public void playedWithTest() {
//        User u = new User();
//        UserProfile a = new UserProfile();
//        u.setPlayedWith(a);
//        Assert.assertEquals(a, u.getPlayedWith());
//    }
//    
    @Test
    public void profileTest() {
        User u = new User();
        UserProfile a = new UserProfile();
        u.setProfile(a);
        Assert.assertEquals(a, u.getProfile());
    }
    
   
    @Test
    public void serialVersionUIDTest() {
        User u = new User();
    	long a = 1L;
        Assert.assertEquals(a, u.getSerialversionuid());
    }

}