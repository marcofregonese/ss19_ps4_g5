package at.qe.sepm.skeleton.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.util.List;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AnswerTest {

    @Test
    public void setIdTest() {
        Answer answer = new Answer();
        Long a = 0L;
        answer.setId(a);
        Assert.assertEquals(a, answer.getId());
    }
    
    @Test
    public void setAnswerTest() {
        Answer answer = new Answer();
        String a = "tester";
        answer.setAnswer(a);
        Assert.assertEquals(a, answer.getAnswer());
    }

    @Test
    public void setDifficultyTest() {
        Answer answer = new Answer();
        String a = "tester";
        answer.setDifficulty(a);
        Assert.assertEquals(a, answer.getDifficulty());
    }
    
    @Test
    public void setTopicTest() {
        Answer answer = new Answer();
        String a = "tester";
        answer.setTopic(a);
        Assert.assertEquals(a, answer.getTopic());
    }
    
    @Test
    public void setPoolTagTest() {
        Answer answer = new Answer();
        Pool a = new Pool();
        answer.setPool(a);
        Assert.assertEquals(a, answer.getPool());
    }
    
    @Test
    public void setQuestionTest() {
        Answer answer = new Answer();
        List<Question> a = null;
        answer.setQuestion(a);
        Assert.assertEquals(a, answer.getQuestion());
    }
    


}