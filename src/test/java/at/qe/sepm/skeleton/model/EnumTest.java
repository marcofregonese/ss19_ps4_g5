package at.qe.sepm.skeleton.model;

import at.qe.sepm.skeleton.model.GameRoomSate;
import at.qe.sepm.skeleton.model.GameMode;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class EnumTest {
    @Test
    public void GameRoomSateTeamTest() {
        Assert.assertEquals("team", GameRoomSate.TEAM_BUILDING.getTest());
    }
    @Test
    public void GameRoomSateReadyTest() {
        Assert.assertEquals("ready", GameRoomSate.READY.getTest());
    } 
    @Test
    public void GameRoomSateRunningTest() {
        Assert.assertEquals("running", GameRoomSate.RUNNING.getTest());
    }
    @Test
    public void GameRoomSatePausedTest() {
        Assert.assertEquals("paused", GameRoomSate.PAUSED.getTest());
    }
    @Test
    public void GameRoomSateCompletedTest() {
        Assert.assertEquals("completed", GameRoomSate.COMPLETED.getTest());
    }
  
    
    @Test
    public void GameModeQuestionTest() {
        Assert.assertEquals("Fragen", GameMode.QUESTION.getTest());
    }

    @Test
    public void GameModeAnswerTest() {
        Assert.assertEquals("Antworten", GameMode.ANSWER.getTest());
    }
}
