package at.qe.sepm.skeleton.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.util.List;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class PoolTest {

	@Test
	public void setIdTest() {
		Pool pool = new Pool();
		Long a = 0L;
		pool.setId(a);
		Assert.assertEquals(a, pool.getId());
	}

	@Test
	public void setDifficultyTest() {
		Pool pool = new Pool();
		String a = "tester";
		pool.setDifficulty(a);
		Assert.assertEquals(a, pool.getDifficulty());
	}

	@Test
	public void setTopicTest() {
		Pool pool = new Pool();
		String a = "tester";
		pool.setTopic(a);
		Assert.assertEquals(a, pool.getTopic());
	}

	@Test
	public void setTotalPointsTest() {
		Pool pool = new Pool();
		Integer a = 0;
		pool.setTotalPoints(a);
		Assert.assertEquals(a, pool.getTotalPoints());
	}
	
	@Test
	public void setGameroomsTest() {
		Pool pool = new Pool();
		List<GameRoom> a = null;		
		pool.setGamerooms(a);
		Assert.assertEquals(a, pool.getGamerooms());
	}
	
    @Test
    public void setQuestionsTest() {
		Pool pool = new Pool();
        List<Question> a = null;
        pool.setQuestions(a);
        Assert.assertEquals(a, pool.getQuestions());
    }
	
    @Test
    public void setAnswersTest() {
		Pool pool = new Pool();
        List<Answer> a = null;
        pool.setAnswers(a);
        Assert.assertEquals(a, pool.getAnswers());
    }
	@Test
	public void setAuthorTest() {
		Pool pool = new Pool();
		User a = new User();
		pool.setAuthor(a);
		Assert.assertEquals(a, pool.getAuthor());
	}
    
}