package at.qe.sepm.skeleton.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.util.List;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TopicExtensionTest {

    @Test
    public void setIdTest() {
        TopicExtension tp = new TopicExtension();
        Long a = 0L;
        tp.setId(a);
        Assert.assertEquals(a, tp.getId());
    }
    
    @Test
    public void setTopicNameTest() {
        TopicExtension tp = new TopicExtension();
    	String a = "tester";
        tp.setTopicName(a);
        Assert.assertEquals(a, tp.getTopicName());
    }
    
    @Test
    public void isNewTest() {
        TopicExtension tp = new TopicExtension();
        boolean a = false;
        Assert.assertEquals(a, tp.isNew());
    }
    
    @Test
    public void serialVersionUIDTest() {
        TopicExtension tp = new TopicExtension();
    	long a = 1L;
        Assert.assertEquals(a, tp.getSerialversionuid());
    }

}