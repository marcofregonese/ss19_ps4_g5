/**
 * 
 */
package at.qe.sepm.skeleton.model;

import java.time.LocalTime;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostRemove;
import javax.persistence.PreRemove;


import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.domain.Persistable;

/**
 * @author marco
 *
 */
@Entity
public class Pool implements Persistable<Long> {
	
    private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(updatable = false, nullable = false)
	private Long id;
	
	@Column
	private String difficulty;
	
	@Column
	private String topic;
    
    @Column
    private LocalTime startTime;
    
    @Column
    private LocalTime endTime;
    
    @Column
    private Integer totalPoints;
    
    @ManyToMany(mappedBy = "pools")
    List<GameRoom> gamerooms;
    
    @ManyToMany(cascade = {CascadeType.ALL} , mappedBy = "poolTag")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Question> questions;
    
    @ManyToMany(cascade = {CascadeType.ALL} , mappedBy = "poolTag")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Answer> answers;
    
    @ManyToOne
    @JoinColumn(name = "username")
    private User author;
    
    @PreRemove
	public void removePoolFromUser() {
		this.getAuthor().getPool().remove(this);
	}

    
	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public List<GameRoom> getGamerooms() {
		return gamerooms;
	}

	public void setGamerooms(List<GameRoom> gamerooms) {
		this.gamerooms = gamerooms;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public Integer getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(Integer totalPoints) {
		this.totalPoints = totalPoints;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public Long getId() {
		return id;
	}

	@Override
	public boolean isNew() {
		//TODO overwrite 
		return false;
	}

}
