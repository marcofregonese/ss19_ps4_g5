/**
 * 
 */
package at.qe.sepm.skeleton.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.domain.Persistable;

/**
 * @author marco
 *
 */
@Entity
public class GameRoom implements Persistable<Long>{
	
    private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "gameRoomId", updatable = false, nullable = false)
	private Long id;
	
    @Column
    private LocalDate startDate;
    @Column
    private LocalDate endDate;
    @Column
    private LocalTime startTime;
    @Column
    private LocalTime endTime;
    
    @CollectionTable(name = "GameRoomstate")
    @Enumerated(EnumType.STRING)
    private GameRoomSate state;
    
    @Column
    private String teamName;
    
    @Column
    private Integer noRightSolution;
    
    @ManyToMany(mappedBy = "gameRooms")
    @LazyCollection(LazyCollectionOption.FALSE)
    List<User> users;
    
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable( name = "gameroom_pool", joinColumns = @JoinColumn(name = "gameroom_id"), inverseJoinColumns = @JoinColumn(name = "pool_id"))
    @LazyCollection(LazyCollectionOption.FALSE)
    List<Pool> pools;

	public List<Pool> getPools() {
		return pools;
	}

	public void setPools(List<Pool> pools) {
		this.pools = pools;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public GameRoomSate getState() {
		return state;
	}

	public void setState(GameRoomSate state) {
		this.state = state;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Integer getNoRightSolution() {
		return noRightSolution;
	}

	public void setNoRightSolution(Integer noRightSolution) {
		this.noRightSolution = noRightSolution;
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return false;
	}
}
