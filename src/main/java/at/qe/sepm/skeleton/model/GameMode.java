/**
 * 
 */
package at.qe.sepm.skeleton.model;

/**
 * @author marco
 *
 */
public enum GameMode {

	QUESTION("Fragen"),
	ANSWER("Antworten");
	
	private String test;

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}
	
	GameMode (String test){
		this.test = test;
	}
}


