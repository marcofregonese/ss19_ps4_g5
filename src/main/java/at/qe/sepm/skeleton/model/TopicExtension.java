/**
 * 
 */
package at.qe.sepm.skeleton.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.domain.Persistable;

/**
 * @author marco, Lukas
 * 
 * Enumeration of available topics
 *
 */
@Entity
public class TopicExtension  implements Persistable<Long> {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TopicExtensionId", updatable = false, nullable = false)
	private Long id;
	
	@Column(name="TopicName", nullable = false, unique=true)
	private String TopicName;

	/**
	 * @return the topicName
	 */
	public String getTopicName() {
		return TopicName;
	}

	/**
	 * @param topicName the topicName to set
	 */
	public void setTopicName(String topicName) {
		TopicName = topicName;
	}

	public void setId(Long id) {
		this.id=id;
	}
	
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
