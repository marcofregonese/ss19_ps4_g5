/**
 * 
 */
package at.qe.sepm.skeleton.model;

/**
 * @author marco
 *
 */
public enum GameRoomSate {
	
	TEAM_BUILDING("team"),
	READY("ready"),
	RUNNING("running"),
	PAUSED("paused"),
	COMPLETED("completed");

	
	private String test;

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}
	
	GameRoomSate (String test){
		this.test = test;
	}
	
}
