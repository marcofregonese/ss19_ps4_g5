/**
 * 
 */
package at.qe.sepm.skeleton.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.domain.Persistable;

/**
 * @author marco
 *
 */
@Entity(name = "Question")
public class Question implements Persistable<Long>  {

    private static final long serialVersionUID = 1L;
    
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "questionId", updatable = false, nullable = false)
	private Long id;

	@Column
	private String question;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable( name = "question_answer", joinColumns = @JoinColumn(name = "question_id"), inverseJoinColumns = @JoinColumn(name = "answer_id"))
	private List<Answer> answers;

	
	@Column
	private String difficulty;
	
	@Column
	private String topic;
	
	@Column
	private String video;
	

	@ManyToOne
    @JoinColumn(name = "pool_id")
    private Pool poolTag;
	
	
	@PreRemove
	public void removeQuestionFromPool() {
		this.getPool().getQuestions().remove(this);
	}
    
    

	public Pool getPool() {
		return poolTag;
	}

	public void setPool(Pool pool) {
		this.poolTag = pool;
	}



	public void setId(Long id) {
		this.id = id;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}


	public String getVideo() {
		return video;
	}



	public void setVideo(String video) {
		this.video = video;
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Long getId() {
		return id;
	}
	
}
