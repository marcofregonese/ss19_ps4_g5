package at.qe.sepm.skeleton.model;

	import java.time.Duration;
	import java.util.List;

	import javax.persistence.CascadeType;
	import javax.persistence.Column;
	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;
	import javax.persistence.JoinColumn;
	import javax.persistence.ManyToOne;
	import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.PreRemove;

import org.hibernate.annotations.LazyCollection;
	import org.hibernate.annotations.LazyCollectionOption;
	import org.springframework.data.domain.Persistable;




@Entity
public class UserProfile implements Persistable<Long>{

	

		
		private static final long serialVersionUID = 1L;
		
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name = "userProfileId", updatable = false, nullable = false)
		private Long id;
		
		
		@Column
		private int rightPools;
		
		@Column
		private long totalTime;
		
		@Column
		private int noSucceed;
		
		@Column
		private int noFailed;
		
		@Column
		private String playedWith;

		
		@OneToOne
		@JoinColumn(name = "username")
		private User owner;
		

		
		
		
		public int getRightPools() {
			return rightPools;
		}

		public void setRightPools(int rightPools) {
			this.rightPools = rightPools;
		}

		

		public String getTotalTimeString() {
			long seconds = totalTime / 1000;
			long minutes = seconds / 60;
			seconds = seconds % 60;
			long hours = minutes/60;
			minutes = minutes % 60;
			long days = hours / 24;
			hours = hours % 24;
			return String.format("Days: %d Hours: %d Minutes: %d Seconds: %d", days, hours, minutes, seconds);
		}
		
		@PreRemove
		public void removePoolFromUser() {
			this.getOwner().setProfile(null);
		}
			
		
		public long getTotalTime() {
			return totalTime;
		}


		public void setTotalTime(long totalTime) {
			this.totalTime = totalTime;
		}

		public int getNoSucceed() {
			return noSucceed;
		}

		public void setNoSucceed(int noSucceed) {
			this.noSucceed = noSucceed;
		}

		public int getNoFailed() {
			return noFailed;
		}

		public void setNoFailed(int noFailed) {
			this.noFailed = noFailed;
		}

		public String getPlayedWith() {
			return playedWith;
		}

		public void setPlayedWith(String playedWith) {
			this.playedWith = playedWith;
		}


		public User getOwner() {
			return owner;
		}

		public void setOwner(User owner) {
			this.owner = owner;
		}

		public void setId(Long id) {
			this.id=id;
		}
		
		
		@Override
		public Long getId() {
			return id;
		}

		@Override
		public boolean isNew() {
			// TODO Auto-generated method stub
			return false;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		

	}

