/**
 * 
 */
package at.qe.sepm.skeleton.model;

import java.util.ArrayList;
import java.util.*;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;

import org.apache.tomcat.util.codec.binary.Base64;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.domain.Persistable;


/**
 * @author marco
 *
 */
@Entity(name = "Answer")
public class Answer implements Persistable<Long> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "answerId", updatable = false, nullable = false)
	private Long id;

	@Column
	private String answer;

	@ManyToMany(mappedBy = "answers")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Question> questions;


	@Column
	private String difficulty;

	@Column
	private String topic;

	@ManyToOne
	@JoinColumn(name = "pool_id")
	private Pool poolTag;
	



	public Pool getPool() {
		return poolTag;
	}

	public void setPool(Pool pool) {
		this.poolTag = pool;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Question> getQuestion() {
		return questions;
	}

	public void setQuestion(List<Question> questions) {
		this.questions = questions;
	}

	@PreRemove
	public void removeAnswerFromPool() {
		this.getPool().getAnswers().remove(this);
	}

	@Override
	public boolean isNew() {
		return false;
	}


	public Long getId() {
		return id;
	}
	


}
