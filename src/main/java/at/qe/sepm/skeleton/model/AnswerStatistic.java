/**
 * 
 */
package at.qe.sepm.skeleton.model;

import java.time.Duration;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.domain.Persistable;

/**
 * @author marco
 *
 */
@Entity
public class AnswerStatistic implements Persistable<Long>{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "answerStatisticId", updatable = false, nullable = false)
	private Long id;
	
	
	@Column
	private String topic;
	
	@Column
	private String difficulty;
	
	@Column
	private Integer noSucceed;
	
	@Column
	private Integer noFailed;
	
	@Column
	private Double percentage;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "username")
	private User player;
	

	public Integer getNoSucceed() {
		return noSucceed;
	}

	public void setNoSucceed(Integer noSucceed) {
		this.noSucceed = noSucceed;
	}

	public Integer getNoFailed() {
		return noFailed;
	}

	public void setNoFailed(Integer noFailed) {
		this.noFailed = noFailed;
	}


	public User getPlayer() {
		return player;
	}

	public void setPlayer(User player) {
		this.player = player;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return false;
	}

}
