import os
import os.path

arr=[os.path.join(root, f) for root, _, files in os.walk('.') for f in files if f.endswith(".java")]
for a in arr:
	os.rename( a, a+"~" )
	destination= open( a, "w" )
	source= open( a+"~", "r" )
	for line in source:
		if line.startswith("package"):
			destination.write("package at.qe.sepm.skeleton."+line[line.find("validator"):])
		else:
			destination.write(line)
	destination.close()
	source.close()
	os.remove( a+"~")