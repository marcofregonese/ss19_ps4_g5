package at.qe.sepm.skeleton;

import at.qe.sepm.skeleton.configs.CustomServletContextInitializer;
import at.qe.sepm.skeleton.configs.WebSecurityConfig;
import at.qe.sepm.skeleton.ui.controllers.Endpoints;
//import at.qe.sepm.skeleton.game.Endpoint;
import at.qe.sepm.skeleton.utils.ViewScope;
import java.util.HashMap;
import javax.faces.webapp.FacesServlet;
import javax.websocket.server.ServerEndpointConfig;

import org.primefaces.push.PushServlet;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;


/**
 * Spring boot application. Execute maven with <code>mvn spring-boot:run</code>
 * to start this web application.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class Main extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    	//ServerEndpointConfig.Builder.create(Endpoints.class, "/game/{gameid}/{user}").build();
    	return application.sources(new Class[]{Main.class, CustomServletContextInitializer.class, WebSecurityConfig.class});
    }

    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        FacesServlet servlet = new FacesServlet();
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(servlet, "*.xhtml");
        return servletRegistrationBean;
    }
    
    @Bean
    public ServletRegistrationBean pushServletRegistration(){
        ServletRegistrationBean pushServlet = new ServletRegistrationBean(new PushServlet(), "/primepush/*");
        pushServlet.addInitParameter("org.atmosphere.annotation.packages", "org.primefaces.push");
        pushServlet.addInitParameter("org.atmosphere.cpr.packages", "at.qe.sepm.skeleton");
        pushServlet.addInitParameter("org.atmosphere.cpr.broadcasterCacheClass", "org.atmosphere.cache.UUIDBroadcasterCache");
        pushServlet.setAsyncSupported(true);
        pushServlet.setLoadOnStartup(1);
        pushServlet.setName("Push Servlet");
        pushServlet.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return pushServlet;
    }


    @Bean
    public CustomScopeConfigurer customScopeConfigurer() {
        CustomScopeConfigurer customScopeConfigurer = new CustomScopeConfigurer();
        HashMap<String, Object> customScopes = new HashMap<>();
        customScopes.put("view", new ViewScope());
        customScopeConfigurer.setScopes(customScopes);
        return customScopeConfigurer;
    }

}
