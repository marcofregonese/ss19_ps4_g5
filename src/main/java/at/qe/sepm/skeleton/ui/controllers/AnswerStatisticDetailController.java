package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.repositories.UserRepository;
import at.qe.sepm.skeleton.services.AnswerService;
import at.qe.sepm.skeleton.services.AnswerStatisticService;
import at.qe.sepm.skeleton.services.QuestionService;
import at.qe.sepm.skeleton.services.UserService;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("view")
public class AnswerStatisticDetailController {

	
	 @Autowired
	   private UserService userService;
	 @Autowired
	 private AnswerStatisticService answerStatisticService;
	
	private User user;
	private User currentUser;
	boolean check = true;
	
	
	public User getCurrentUser() {
		if(check){
			check = false;
			this.currentUser = userService.getAuthenticatedUser();
		}
		return currentUser;
	}
	
	public List<AnswerStatistic> lastGamesPlayedAllStatistics(){
		List<AnswerStatistic> lastPlayed = new ArrayList<AnswerStatistic>();
		List<AnswerStatistic> UserStatisticList = new ArrayList<>();
			if(user != null){
		UserStatisticList = user.getAnswerStatistics();
		for(int i = UserStatisticList.size() - 3; i < UserStatisticList.size(); i++){
			if(i < 0){
				i++;
				continue;
			}
		lastPlayed.add(UserStatisticList.get(i));
		}
			
			
		UserStatisticList = new ArrayList<>();
		UserStatisticList.addAll(lastPlayed);
		
		for(int i = 0; i < UserStatisticList.size(); i++){
			lastPlayed.set(i, UserStatisticList.get(UserStatisticList.size()-1-i));
		}
			}
		return lastPlayed;
	}
	
	public List<AnswerStatistic> getAnswerStatisticsAllStatistics() {
		return answerStatisticService.getAnswerStatistics(user);
	}

	public List<AnswerStatistic> lastGamesPlayed(){
		this.currentUser = userService.getAuthenticatedUser();
		List<AnswerStatistic> lastPlayed = new ArrayList<AnswerStatistic>();
		List<AnswerStatistic> UserStatisticList = currentUser.getAnswerStatistics();
		for(int i = UserStatisticList.size() - 3; i < UserStatisticList.size(); i++){
			if(i < 0){
				i++;
				continue;
			}
		lastPlayed.add(UserStatisticList.get(i));
		}
			
			
		UserStatisticList = new ArrayList<>();
		UserStatisticList.addAll(lastPlayed);
		
		for(int i = 0; i < UserStatisticList.size(); i++){
			lastPlayed.set(i, UserStatisticList.get(UserStatisticList.size()-1-i));
		}
			
		return lastPlayed;
	}
	
	public List<AnswerStatistic> getAnswerStatistics() {
		this.currentUser=userService.getAuthenticatedUser();
		return answerStatisticService.getAnswerStatistics(currentUser);
	}

	public User loadCurrentUser() {
		return userService.getAuthenticatedUser();
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
        this.user = user;
        doReloadUser();
    }
	
	public void doReloadUser() {
        user = userService.loadUser(user.getUsername());
    }
		
}
