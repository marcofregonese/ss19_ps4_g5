package at.qe.sepm.skeleton.ui.beans;

import java.security.SecureRandom;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.UserService;

/**
 * Simple Passwordgenerator as java bean.
 * 
 * @author Lukas
 *
 */
@Component
@Scope("view")
public class PasswordGeneratorBean {

	@Autowired
	private UserService userService;

	@Autowired
	BCryptPasswordEncoder encoder;

	private String newPassword;
	private String confirmPassword;

	private User user;

	private SecureRandom rand;
	private String lowercaseLetters = "abcdefghijklmnopqrstuvwxyz";
	private String uppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private String mutadedVowelsLowerCases = "äöü";
	private String mutadedVowelsUpperCases = "ÄÖÜ";
	private String digits = "0123456789";
	private String specialSigns = "!?.;-_#@$";
	private int len;
	private boolean useUpperCaseLetters = true;
	private boolean usemutadedVowels = true;
	private boolean useDigits = true;
	private boolean useSpecialSigns = true;

	private boolean showPassword;
	private String generatedPassword;

	public PasswordGeneratorBean() {
		len=5;
		rand = new SecureRandom();
	}

	/**
	 * Generates a new Password.
	 */
	public void generatePassword() {
		generatedPassword = getPassword(len, useUpperCaseLetters, usemutadedVowels, useDigits, useSpecialSigns);
	}

	/**
	 * Returns a new password.
	 * 
	 * @param length
	 *            Length of the password, at least 5.
	 * @param uses
	 *            what do use other than lower case letters 0 uppercaseLetters,
	 *            1 mutated vowels (same settings as normal letters), 2 digits,
	 *            3 special signs, as default it is assumed that all properties
	 *            are used.
	 * @return
	 */
	public String getPassword(int length, boolean... uses) {
		useUpperCaseLetters = true;
		usemutadedVowels = true;
		useDigits = true;
		useSpecialSigns = true;
		if (uses.length >= 1) {
			useUpperCaseLetters = uses[0];
			if (uses.length >= 2) {
				usemutadedVowels = uses[1];
				if (uses.length >= 3) {
					useDigits = uses[2];
					if (uses.length >= 4) {
						useSpecialSigns = uses[3];
					}
				}
			}
		}
		// umv for used mutated vowels
		String umv = (useUpperCaseLetters) ? mutadedVowelsLowerCases + mutadedVowelsUpperCases
				: mutadedVowelsLowerCases;
		length = (length < 5) ? 5 : length;
		StringBuilder sb = new StringBuilder();
		while (sb.length() < length) {
			int whatToInput = rand.nextInt(5);
			;
			switch (whatToInput) {
			case 0:
				sb.append(randomOneLetterSubstring(lowercaseLetters));
				break;
			case 1:
				if (useUpperCaseLetters) {
					sb.append(randomOneLetterSubstring(uppercaseLetters));
					break;
				}
			case 2:
				if (usemutadedVowels) {
					sb.append(randomOneLetterSubstring(umv));
					break;
				}
			case 3:
				if (useDigits) {
					sb.append(randomOneLetterSubstring(digits));
					break;
				}
			default:
				if (useSpecialSigns) {
					sb.append(randomOneLetterSubstring(specialSigns));
				}
			}

		}
		return sb.toString();
	}

	private String randomOneLetterSubstring(String str) {
		if (str.equals(null)) return null;
		int index = rand.nextInt(str.length());
		return str.substring(index, index + 1);
	}

	/**
	 * Updates the Password encode.
	 * 
	 * @param newPwd
	 */
	@SuppressWarnings("deprecation")
	public void updatePassword() {
		if (newPassword.equals(confirmPassword)) {
			user.setPassword(encoder.encode(newPassword));
			user = userService.saveUser(user);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Your password has been changed successfully!"));
		} else {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR",
					"Password should match with Confirm Password!");
			RequestContext.getCurrentInstance().showMessageInDialog(message);
		}
	}
	
	/**
	 * Saves the password that was previously generated
	 */
	@SuppressWarnings("deprecation")
	public void saveGeneratedPassword(){
		if(generatedPassword==null || generatedPassword.isEmpty()){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR",
					"Please generate your Password first!");
			RequestContext.getCurrentInstance().showMessageInDialog(message);
		}else{
			user.setPassword(encoder.encode(generatedPassword));
			user = userService.saveUser(user);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Your password has been changed successfully!"));
		}
	}
	
	@PostConstruct
	public void setAuthenticatedUser() {
		user = userService.getAuthenticatedUser();
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * @param userService the userService to set
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * @param confirmPassword the confirmPassword to set
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the lowercaseLetters
	 */
	public String getLowercaseLetters() {
		return lowercaseLetters;
	}

	/**
	 * @param lowercaseLetters the lowercaseLetters to set
	 */
	public void setLowercaseLetters(String lowercaseLetters) {
		this.lowercaseLetters = lowercaseLetters;
	}

	/**
	 * @return the uppercaseLetters
	 */
	public String getUppercaseLetters() {
		return uppercaseLetters;
	}

	/**
	 * @param uppercaseLetters the uppercaseLetters to set
	 */
	public void setUppercaseLetters(String uppercaseLetters) {
		this.uppercaseLetters = uppercaseLetters;
	}

	/**
	 * @return the mutadedVowelsLowerCases
	 */
	public String getMutadedVowelsLowerCases() {
		return mutadedVowelsLowerCases;
	}

	/**
	 * @param mutadedVowelsLowerCases the mutadedVowelsLowerCases to set
	 */
	public void setMutadedVowelsLowerCases(String mutadedVowelsLowerCases) {
		this.mutadedVowelsLowerCases = mutadedVowelsLowerCases;
	}

	/**
	 * @return the mutadedVowelsUpperCases
	 */
	public String getMutadedVowelsUpperCases() {
		return mutadedVowelsUpperCases;
	}

	/**
	 * @param mutadedVowelsUpperCases the mutadedVowelsUpperCases to set
	 */
	public void setMutadedVowelsUpperCases(String mutadedVowelsUpperCases) {
		this.mutadedVowelsUpperCases = mutadedVowelsUpperCases;
	}

	/**
	 * @return the digits
	 */
	public String getDigits() {
		return digits;
	}

	/**
	 * @param digits the digits to set
	 */
	public void setDigits(String digits) {
		this.digits = digits;
	}

	/**
	 * @return the specialSigns
	 */
	public String getSpecialSigns() {
		return specialSigns;
	}

	/**
	 * @param specialSigns the specialSigns to set
	 */
	public void setSpecialSigns(String specialSigns) {
		this.specialSigns = specialSigns;
	}

	/**
	 * @return the len
	 */
	public int getLen() {
		return len;
	}

	/**
	 * @param len the len to set
	 */
	public void setLen(int len) {
		this.len = len;
	}

	/**
	 * @return the useUpperCaseLetters
	 */
	public boolean isUseUpperCaseLetters() {
		return useUpperCaseLetters;
	}

	/**
	 * @param useUpperCaseLetters the useUpperCaseLetters to set
	 */
	public void setUseUpperCaseLetters(boolean useUpperCaseLetters) {
		//System.err.println("Uppercase letters are now "+(useUpperCaseLetters?"":"not")+" used.");
		this.useUpperCaseLetters = useUpperCaseLetters;
	}

	/**
	 * @return the usemutadedVowels
	 */
	public boolean isUsemutadedVowels() {
		return usemutadedVowels;
	}

	/**
	 * @param usemutadedVowels the usemutadedVowels to set
	 */
	public void setUsemutadedVowels(boolean usemutadedVowels) {
		this.usemutadedVowels = usemutadedVowels;
	}

	/**
	 * @return the useDigits
	 */
	public boolean isUseDigits() {
		return useDigits;
	}

	/**
	 * @param useDigits the useDigits to set
	 */
	public void setUseDigits(boolean useDigits) {
		this.useDigits = useDigits;
	}

	/**
	 * @return the useSpecialSigns
	 */
	public boolean isUseSpecialSigns() {
		return useSpecialSigns;
	}

	/**
	 * @param useSpecialSigns the useSpecialSigns to set
	 */
	public void setUseSpecialSigns(boolean useSpecialSigns) {
		this.useSpecialSigns = useSpecialSigns;
	}

	/**
	 * @return the showPassword
	 */
	public boolean isShowPassword() {
		return showPassword;
	}

	/**
	 * @param showPassword the showPassword to set
	 */
	public void setShowPassword(boolean showPassword) {
		this.showPassword = showPassword;
	}

	/**
	 * @return the generatedPassword
	 */
	public String getGeneratedPassword() {
		return generatedPassword;
	}

	/**
	 * @param generatedPassword the generatedPassword to set
	 */
	public void setGeneratedPassword(String generatedPassword) {
		this.generatedPassword = generatedPassword;
	}
}