package at.qe.sepm.skeleton.ui.beans;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("application")
public class GameLogic {

	private volatile Map<Integer,Game> games=new HashMap<Integer,Game>();

	public Map<Integer,Game> getGames() {
		return games;
	}

	public void addGame(int gameId,Game game) {
		this.games.put(gameId, game);
	}

	public void updateGame(int gameId,Game game) {
		this.games.put(gameId, game);
	}

}