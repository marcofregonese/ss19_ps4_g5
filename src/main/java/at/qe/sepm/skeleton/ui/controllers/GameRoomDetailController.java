package at.qe.sepm.skeleton.ui.controllers;


import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.GameMode;

import static java.lang.Math.toIntExact;
import at.qe.sepm.skeleton.model.GameRoom;

import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.GameRoomService;
import at.qe.sepm.skeleton.services.PoolService;
import at.qe.sepm.skeleton.services.QuestionService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.Game;
import at.qe.sepm.skeleton.ui.beans.GameLogic;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */

@Component
@Scope("view")


public class GameRoomDetailController {


	@Autowired
	private GameRoomService gameRoomService;
	@Autowired
	private PoolService poolService = new PoolService();
	@Autowired
	private UserService userService = new UserService();
	@Autowired
	private QuestionService questionService;
	@Autowired
	private SessionInfoBean sessionInfoBean;
	@Autowired
	private GameLogic gameLogic;

	private Game g;


	/**
	 * Attribute to cache the currently displayed user
	 */
	private GameRoom gameRoom=new GameRoom();
	private long grid;
	private List<Pool> poollist = new ArrayList<Pool>();
	private String selectedPools;
	private String selectedHurn = null;
	private List<User> mo;
	private List<Question> questions=new ArrayList<Question>();
	private Iterator<Question> iterator;
	private boolean gameStarted=false;
	private User user;
	private String numberAnswers;
	private int gameMode;
	private int maxPlayers;



	public int getGameMode() {
		return gameMode;
	}


	public void setGameMode(int gameMode) {
		this.gameMode = gameMode;
	}


	public void setSelectedHurn(String selectedHurn) {
		this.selectedHurn = selectedHurn;
	}


	public String getSelectedHurn() {
		return this.selectedHurn;
	}
	public void setGameRoom(GameRoom gameRoom) {
		this.gameRoom = gameRoom;
	}

	/**
	 * Returns the currently displayed user.
	 *
	 * @return
	 */
	public GameRoom getGameRoom() {
		return gameRoom;
	}

	public void setPoolList(List<Pool> poollist) {
		this.poollist = poollist;
		this.gameRoom.setPools(poollist);
		this.gameRoomService.saveGameRoom(gameRoom);
		System.out.println(poollist);
	}

	public List<Pool> getPoolList(){
		return poollist;
	}

	/**
	 * Action to save the currently displayed user.
	 */
	public String doSaveGameRoom() {
		//System.out.println(gameRoom.getPools().get(0));
		//gameRoom = new GameRoom();
		this.gameRoom = this.gameRoomService.saveGameRoom(gameRoom);
		grid=this.gameRoom.getId();
		//System.out.println(gameRoom);	




		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName(); //get logged in username
		User u=this.userService.loadUser(name);
		u.setRole(UserRole.TEAMLEADER);
		this.userService.saveUser(u);


		this.doSaveGameRoomPools(selectedPools);
		//this.doSearchById();

		Game g=new Game();
		g.addPools(gameRoom.getPools());
		if(gameRoom.getPools().get(0).getId()==6)
			g.setNumberAnswers(Integer.parseInt(numberAnswers));
		else
			g.setNumberAnswers(4);
		g.setCurrentPoolId(0);
		if(gameMode==0)
			g.setGameMode(GameMode.QUESTION);
		else
			g.setGameMode(GameMode.ANSWER);
		g.setMaxPlayers(maxPlayers);
		gameLogic.addGame(toIntExact(grid), g);

		this.doSaveById();
		System.out.println(this.gameRoom.getId()+" "+this.gameRoom.getTeamName());
		//return this.execute();

		return this.execute();
	}

	public void doSearchById() {
		GameRoom gr = gameRoomService.getById(grid);		


	}

	public String doSaveById() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName(); //get logged in username

		//this.labGroup.setUser(userService.loadUser(name));
		this.gameRoom = gameRoomService.getById(grid);



		User user = this.userService.loadUser(name);
		user.setGameRooms(gameRoom);
		user=this.userService.saveUser(user);


		this.gameRoom=gameRoomService.getById(grid);
		g=gameLogic.getGames().get(toIntExact(grid));
		if(g.getUsers().size()<g.getMaxPlayers()) {
			g.addUser(user);
			g.getUserRightAnswers().put(user, 0);
			g.getUserWrongAnswers().put(user, 0);
			g.getUserQuestions().put(user, new Question());
			g.getUserAnswers().put(user, new ArrayList<Answer>());
			g.getUserBlocked().put(user, false);
			
			//g.setNumberAnswers(Integer.valueOf(numberAnswers));
			gameLogic.addGame(toIntExact(grid), g);

			//System.out.println(this.gameRoom);
			return this.execute();
		}
		else
			return "";

	}


	public void doSaveGameRoom2() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName(); //get logged in username


		System.out.println("Gameroomid: "+this.gameRoom.getId());
		mo = this.gameRoom.getUsers();
		System.out.println("Mo "+mo);
		User a = this.userService.loadUser(name);
		System.out.println("a "+ a);

		mo.add(a);

		this.gameRoom.setUsers(mo);

		gameRoom = this.gameRoomService.saveGameRoom(gameRoom);


		this.doSaveGameRoomPools(selectedPools);

	}




	public void doSaveGameRoomPools(String selectedPools) {

			int poolId = Integer.parseInt(selectedPools.substring(0, selectedPools.indexOf(" ")));
			System.out.println("PoolId ind doSaveGameRoomPools: "+poolId);
			Pool furtherEquipment = this.poolService.getByID(poolId);
			poollist.add(furtherEquipment);

		
		this.gameRoom.setPools(poollist);
		this.gameRoom=this.gameRoomService.saveGameRoom(gameRoom);

	}


	//	public String toString(){
	//		return this.pdc.getPool().getId() + this.pdc.getPool().getTopic();
	//	}
	public void setGrid(long grid) {
		this.grid = grid;
	}
	public long getGrid() {
		return this.grid;
	}


	public void init(long id) {
		this.gameRoom=this.gameRoomService.getById(id);
		this.setGameRoom(this.gameRoom);
		System.out.println("init:"+this.gameRoom.getId());
	}
	//
	//	public List<Pool> allpools(String id){
	//		if(id==null)
	//			System.out.println("disgusting");
	//		this.gameRoom=this.gameRoomService.getById(Long.valueOf(id));
	//		return this.gameRoom.getPools();
	//
	//	}



	public List<User> getGameroomUsers(){
		return this.gameRoomService.getById(grid).getUsers();
	}

	public boolean isTeamleader() {
		this.user=sessionInfoBean.getCurrentUser();
		if(this.user.getRoles().contains(UserRole.TEAMLEADER)) {
			return true;
		}

		else 
			return false;

	}

	public Question getNextQuestion() {
		if(questions.isEmpty()) {
			//System.out.println("QUESTION EMPTY");
			questions=this.questionService.findByPool(poollist.get(0));
			System.out.println(questions.size());
			//iterator=questions.iterator();
		}
		//else
		//System.out.println("QUESTION NOT EMPTY");
		return questions.get(0);
	}

	public void listenForGamestart() {
		gameStarted=true;
	}


	public void setSelectedPools(String selectedPools) {
		this.selectedPools = selectedPools;
	}

	public String getSelectedPools(){
		return selectedPools;
	}
	public String leaveGame() {
		this.user=sessionInfoBean.getCurrentUser();
		this.user.setGameRooms(null);
		return "secured/welcome.xhtml?faces-redirect=true&includeViewParams=true";
	}

	public String execute() {
		// ...
		return "gameroom?faces-redirect=true&includeViewParams=true";
	}
	public String redirectToGame() {
		// ...
		return "game?faces-redirect=true&includeViewParams=true";
	}

	//	public boolean reverseAvailable() {
	//		if(selectedPools!=null) {
	//		if(selectedPools.contains("Computerspiele"))
	//			return true;
	//		else
	//			return false;
	//		}
	//		else
	//			return true;
	//	}

	public String getNumberAnswers() {
		return numberAnswers;
	}


	public void setNumberAnswers(String numberAnswers) {
		this.numberAnswers = numberAnswers;
	}


	public int getMaxPlayers() {
		return maxPlayers;
	}


	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

}
