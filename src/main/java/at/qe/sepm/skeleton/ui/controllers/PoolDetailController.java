package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.AnswerService;
import at.qe.sepm.skeleton.services.PoolService;
import at.qe.sepm.skeleton.services.QuestionService;
import at.qe.sepm.skeleton.services.UserService;

import java.util.*;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class PoolDetailController {

	@Autowired
	private PoolService poolService;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private AnswerService answerService;
	@Autowired
	private UserService userService;

	private Question questionDelete;



	private Pool currentPool;
	private Iterator<Question> iterator;
	private Pool pool = new Pool();
	private List<Question> questions = new ArrayList<Question>();
	private List<Answer> answers  = new ArrayList<Answer>();
	private Question question = new Question();
	private Answer answer = new Answer();
	private int amount = 3;
	private boolean drei = true;
	private boolean vier = false;
	private boolean funf = false;
	private Question question123;
	private boolean check = false;
	
	private String test;
	
	public Question getQuestion123() {
		return question123;
	}

	public void setQuestion123(Question question123) {
		this.question123 = question123;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
		if(!(test.equals(""))) {
		this.answer.setAnswer(test);
		this.answers.add(answer);
		this.answer = new Answer();
		}
	}
	
	public String getTester() {
		return test;
	}

	public void setTester(String test) {
		this.test = test;
		if(!(test.equals(""))) {
		this.question.setQuestion(test);
		this.questions.add(question);
		this.question = new Question();
		}
	}
	
	

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Answer getAnswer() {
		return answer;
	}

	public void hoi() {
		
		this.pool = poolService.getByID(pool.getId());
		Question question123;
		
		for (int i = 0; i < questions.size(); i++) {
			question123 = questions.get(i);
			question123.setDifficulty(pool.getDifficulty());
			question123.setTopic(pool.getTopic());
			question123.setPool(pool);
		}
		
		for (int i = 0; i < answers.size(); i++) {
			Answer answer123 = answers.get(i);
			answer123.setDifficulty(pool.getDifficulty());
			answer123.setTopic(pool.getTopic());
			answer123.setPool(pool);
			answer123 = answerService.saveAnswer(answer123);
			answers.set(i, answer123);
		}

		for(int i = 0; i < questions.size(); i++) {
			question123 = questions.get(i); 
			question123 = questionService.saveQuestion(question123);
			questions.set(i, question123);
		}
		for (Answer answer : answers) {
			answer.setQuestion(questions);
			answerService.saveAnswer(answer);
		}
		for (Question question : questions) {
			question.setAnswers(answers);
			questionService.saveQuestion(question);
		}
		this.pool = poolService.getByID(pool.getId());
		if(check == false)
			doReload();
		questions = new ArrayList<>();
		answers = new ArrayList<>();
		
	}
	
	public void addItems(){
		this.question.setAnswers(answers);
		answer = new Answer();
		question = new Question();
	}
	
	public void doDeletePool() {
		List<Question> questionDelete = currentPool.getQuestions();
		for (Question question : questionDelete) {
			this.questionService.deleteQuestion(question);
		}
		if(check == false){
			doReload();
		}
			
		this.poolService.deletePool(currentPool);
		this.currentPool = null;
	}


	
	public void doCreatePool() {
		this.check = true;
		this.pool.setAuthor(userService.getAuthenticatedUser());
		this.pool = poolService.savePool(pool);
	}
	
	public void doAddQuestionsToPool() {
		this.pool = poolService.getByID(currentPool.getId());
		questions = new ArrayList<>();
		answers = new ArrayList<>();
	}


	public void doSavePool() {
		pool = this.poolService.savePool(pool);
	}
	

    
	
	public void setPool(Pool pool) {
		this.pool=pool;

	}

	public Pool getPool() {
		return pool;
	}

	public Collection<Pool> getPools() {
		return poolService.getAllPools();
	}

	public List<String> toHurn() {
		Collection<Pool> poolhurn = this.poolService.getAllPools();
		List<String> ret = new ArrayList<String>();
		for(Pool hurn : poolhurn) {
			ret.add(hurn.getId()+" "+hurn.getTopic()+" "+hurn.getDifficulty());
		}
		return ret;
	}
	
	
	public boolean showDrei() {
		return drei;
	}
	public boolean showVier() {
		return vier;
	}
	public boolean showFunf() {
		return funf;
	}

	public void setAmount(int amount) {
		this.amount = amount;

		if(amount == 3) {
			drei = true;
			vier = false;
			funf = false;
		}
		else if(amount == 4) {
			drei = true;
			vier = true;
			funf = false;
		}else if(amount == 5) {
			drei = true;
			vier = true;
			funf = true;
		}

	}
	public int getAmount() {
		return amount;
	}



	public Pool getCurrentPool() {
		return currentPool;
	}

	public void setCurrentPool(Pool currentPool) {
		this.currentPool = currentPool;
		
	}
	
	
	public void doReload(){
			this.currentPool = poolService.getByID(currentPool.getId());
	}

	public void doDeleteQuestion() {
		this.questionService.deleteQuestion(questionDelete);
		this.questionDelete = null;
		doReload();
	}

	public Question getQuestionDelete() {
		return questionDelete;
	}

	public void setQuestionDelete(Question questionDelete) {
		this.questionDelete = questionDelete;
	}
	
	public void doReloadQuestion() {
        questionDelete = questionService.findById(questionDelete.getId());
    }

    
    public void doSaveQuestion() {
        questionDelete = this.questionService.saveQuestion(questionDelete);
        for(int i = 0; i < answers.size()-1; i++){
        	answerService.saveAnswer(answers.get(i));
        }
    }
    
    public void doSetAnswers() {
        answers = questionDelete.getAnswers();
        setAmount(answers.size()-1);
    }

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	
    
    
	

	
}
