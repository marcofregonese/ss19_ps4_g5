package at.qe.sepm.skeleton.ui.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.hibernate.mapping.Set;
import org.springframework.beans.factory.annotation.Autowired;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.GameMode;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.AnswerService;
import at.qe.sepm.skeleton.services.QuestionService;

public class Game {

	private List<User> users=new ArrayList<User>();
	private List<Pool> pools=new ArrayList<Pool>();

	private int currentPoolId;
	private HashSet<Integer> alreadyAskedQuestions= new HashSet<Integer>();

	private Map<User,Integer> userRightAnswers=new HashMap<User,Integer>();
	private Map<User,Integer> userWrongAnswers=new HashMap<User,Integer>();
	private boolean active=true;
	private Map<User,Boolean> userBlocked=new HashMap<User,Boolean>();
	private Map<User,Question> userQuestions=new HashMap<User,Question>();
	private Map<User,List<Answer>> userAnswers=new HashMap<User,List<Answer>>();
	private int jokerUsd=0;

	//for reverse gamemode
	private Map<Question,List<Answer>> questionAnswers=new HashMap<Question,List<Answer>>();

	private boolean initialized=false;
	private boolean blocked=false;
	private boolean jokerUsed=false;
	private GameMode gameMode;
	private long time;

	private int numberAnswers;

	@Autowired
	private QuestionService questionService;

	@Autowired
	private AnswerService answerService;

	private int maxPlayers;


	public boolean isInitialized() {
		return initialized;
	}
	public void setInitialized(boolean initialized) {
		this.initialized = initialized;
	}
	public int getNumberAnswers() {
		return numberAnswers;
	}
	public void setNumberAnswers(int numberAnswers) {
		this.numberAnswers = numberAnswers;
	}
	public void addUser(User user) {
		if(user!=null && !users.contains(user)) {
			users.add(user);
		}
	}
	public List<User> getUsers(){
		return users;
	}
	public GameMode getGameMode() {
		return gameMode;
	}
	public HashSet<Integer> getAlreadyAskedQuestions() {
		return alreadyAskedQuestions;
	}
	public void setAlreadyAskedQuestions(HashSet<Integer> alreadyAskedQuestions) {
		this.alreadyAskedQuestions = alreadyAskedQuestions;
	}
	public int getCurrentPoolId() {
		return currentPoolId;
	}
	public void setCurrentPoolId(int poolId) {
		currentPoolId=poolId;
	}
	public void setCurrentPoolId() {
		currentPoolId++;
	}
	public Map<User, Integer> getUserRightAnswers() {
		return userRightAnswers;
	}
	public void setUserRightAnswers(HashMap<User, Integer> userRightAnswers) {
		this.userRightAnswers = userRightAnswers;
	}
	public Map<User, Integer> getUserWrongAnswers() {
		return userWrongAnswers;
	}
	public void setUserWrongAnswers(HashMap<User, Integer> userWrongAnswers) {
		this.userWrongAnswers = userWrongAnswers;
	}
	public void removeUser(User user) {
		if(user!=null) {
			users.remove(user);
		}
	}
	public void addPool(Pool pool) {
		if(pools!=null  && !pools.contains(pool)) {
			pools.add(pool);
		}
	}
	public void removePool(Pool pool) {
		if(pool!=null) {
			pools.remove(pool);
		}
	}
	public List<Pool> getPools(){
		return pools;
	}
	public void addAskedQuestions(Question question) {
		if(question!=null) {
			alreadyAskedQuestions.add(question.getId().intValue());
		}
	}
	public int questionAnswered(Question question, User user) {
		ArrayList<Question> questions=new ArrayList<Question>(pools.get(currentPoolId).getQuestions());
		int rand=(int) (Math.random()*questions.size());
		int i=0;
		if(alreadyAskedQuestions.size()==(users.size()*4)+jokerUsd)
			return -1;
		for(i=0;alreadyAskedQuestions.contains(questions.get((rand+i)%questions.size()).getId().intValue());i++) {
			if(i==questions.size()) {
				return -1;
			}
		}
		this.alreadyAskedQuestions.add(questions.get((rand+i)%questions.size()).getId().intValue());
		return questions.get((rand+i)%questions.size()).getId().intValue();//questionId for next question
	}

	public void changeStatistic(User user, boolean rightAnswer) {
		if(rightAnswer)
			userRightAnswers.put(user, userRightAnswers.get(user)+1);
		else
			userWrongAnswers.put(user, userWrongAnswers.get(user)+1);
	}
	public void addPools(List<Pool> pools) {
		if(pools!=null) {
			this.pools=pools;
		}
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	public Map<User,Boolean> getUserBlocked() {
		return userBlocked;
	}
	public void setUserBlocked(Map<User,Boolean> userBlocked) {
		this.userBlocked = userBlocked;
	}
	public Map<User,List<Answer>> getUserAnswers() {
		return userAnswers;
	}
	public void setUserAnswers(Map<User,List<Answer>> userAnswers) {
		this.userAnswers = userAnswers;
	}
	public Map<User,Question> getUserQuestions() {
		return userQuestions;
	}
	public void setUserQuestions(Map<User,Question> userQuestions) {
		this.userQuestions = userQuestions;
	}
	public void shuffleAnswers(Question question) {
		List<Answer> answers=new ArrayList<Answer>(question.getAnswers());
		Collections.shuffle(answers);
		answers=answers.subList(0, numberAnswers);
		while(!answers.isEmpty()) {
			for(int i=0;i<users.size();i++) {
				if(userAnswers.get(users.get(i)).size()==numberAnswers)
					continue;
				else {
					userAnswers.get(users.get(i)).add(answers.get(0));
					answers.remove(0);
				}
			}

		}

	}
	public void initializeAfterJoker() {
		for(User u:users) 
			userAnswers.get(u).clear();
		jokerUsd=users.size();
		this.initialize();
	}

	public void initialize() {
		List<Question> questions=new ArrayList<Question>();
		List<Answer> answers=new ArrayList<Answer>();
		HashSet<Integer> random=new HashSet<Integer>();
		int rand=-1;

		ArrayList<Question> questionsPool=new ArrayList<Question>(pools.get(this.currentPoolId).getQuestions());
		for(User u:users) {
			while(random.contains(rand) || rand==-1)
				rand=(int) (Math.random()*questionsPool.size());
			random.add(rand);
			questions.add(questionsPool.get(rand));
			this.userQuestions.put(u, questionsPool.get(rand));
			this.alreadyAskedQuestions.add(rand);

		}

		random.clear();
		rand=-1;

		if(gameMode.equals(GameMode.QUESTION)) {
			for(Question q:questions)
				answers.addAll(q.getAnswers());
		}
		else {
			ArrayList<Answer> answersPool=new ArrayList<Answer>(pools.get(this.currentPoolId).getAnswers());
			for(Question q:questions) {
				questionAnswers.put(q, new ArrayList<Answer>());
				questionAnswers.get(q).add(q.getAnswers().get(0));
				answers.add(q.getAnswers().get(0));
				int id=q.getAnswers().get(0).getId().intValue();
				boolean weiter=false;
				for(int i=0;i<3;i++) {
					while(random.contains(rand)  || rand==-1 || weiter || rand==id) {
						weiter=false;
						rand=(int) (Math.random()*answersPool.size());
						for(Answer a : answers) {
							if(a.getQuestion().get(0).equals(answersPool.get(rand).getQuestion().get(0))) {
								weiter=true;

							}
						}
					}
					random.add(rand);
					answers.add(answersPool.get(rand));
					questionAnswers.get(q).add(answersPool.get(rand));
				}
			}
		}

		Collections.shuffle(answers);
		
		while(!answers.isEmpty()) {
			for(int i=0;i<users.size();i++) {
				if(this.userAnswers.get(users.get(i)).size()==numberAnswers)
					continue;
				else {
					this.userAnswers.get(users.get(i)).add(answers.get(0));
					answers.remove(0);
				}
			}
		}
	}
	
	public void reorderAnswers(User user,Question question,Answer answer) {
		List<Answer> answers;
		int rand=-1;
		HashSet<Integer> random=new HashSet<Integer>();

		if(gameMode.equals(GameMode.QUESTION)) {
			answers=new ArrayList<Answer>(question.getAnswers());
		}
		else {
			answers=new ArrayList<Answer>();
			answers.add(question.getAnswers().get(0));
			ArrayList<Answer> answersPool=new ArrayList<Answer>(pools.get(this.currentPoolId).getAnswers());
			questionAnswers.put(question, new ArrayList<Answer>());
			questionAnswers.get(question).add(question.getAnswers().get(0));
			boolean weiter=false;
			int id=question.getAnswers().get(0).getId().intValue();
			for(int i=0;i<3;i++) {
				while(random.contains(rand) || rand==-1 || weiter || rand==id) {
					weiter=false;
					rand=(int) (Math.random()*answersPool.size());
					for(Answer a : answers) {
						if(a.getQuestion().get(0).equals(answersPool.get(rand).getQuestion().get(0))) {
							weiter=true;
						}
					}
				}
				random.add(rand);
				answers.add(answersPool.get(rand));
				questionAnswers.get(question).add(answersPool.get(rand));
			}
		}

		Collections.shuffle(answers);
		Question oldquestion=new Question();
		List<Answer> oldanswers=new ArrayList<Answer>();

		if(gameMode.equals(GameMode.QUESTION)) {
			for(int i=0;i<this.users.size();i++) {
				if(this.userQuestions.get(this.users.get(i)).getAnswers().contains(answer)) {
					oldquestion=this.userQuestions.get(this.users.get(i));
					oldanswers=new ArrayList<Answer>(oldquestion.getAnswers());
					this.userQuestions.put(users.get(i), question);

				}

			}
		}
		else {
			for(int i=0;i<this.users.size();i++) {
				if(questionAnswers.get(this.userQuestions.get(this.users.get(i))).contains(answer)){
					oldquestion=this.userQuestions.get(this.users.get(i));
					oldanswers=new ArrayList<Answer>(questionAnswers.get(oldquestion));
					this.userQuestions.put(users.get(i), question);
					break;
				}
			}
		}

		int index;
		int iterations=0;

		while(!answers.isEmpty()) {
			for(int i=0;i<this.users.size();i++) {
				for(int j=0;j<oldanswers.size();j++) {
					if(this.userAnswers.get(this.users.get(i)).contains(oldanswers.get(j))) {
						index=this.userAnswers.get(this.users.get(i)).indexOf(oldanswers.get(j));
						this.userAnswers.get(this.users.get(i)).set(index, answers.get(0));
						answers.remove(0);
					}
				}
			}
			iterations++;
			if(iterations>200)
				break;
		}

	}
	public boolean isBlocked() {
		return blocked;
	}
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
	public boolean isJokerUsed() {
		return jokerUsed;
	}
	public void setJokerUsed(boolean jokerUsed) {
		this.jokerUsed = jokerUsed;
	}
	public void setGameMode(GameMode gameMode) {
		this.gameMode=gameMode;

	}
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers=maxPlayers;
	}
	public int getMaxPlayers() {
		return maxPlayers;
	}
	
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}

}

