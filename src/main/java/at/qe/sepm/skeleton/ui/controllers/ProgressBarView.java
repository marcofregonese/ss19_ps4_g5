package at.qe.sepm.skeleton.ui.controllers;

import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.springframework.stereotype.Component;

@Component
@ViewScoped
public class ProgressBarView{
     
    private Integer progress;
 
    public Integer getProgress() {
    	if(progress==null)
    		progress=10;
        return progress;
    }
 
    public void decrement() {
        this.progress--;
    }
     
    public boolean end() {
    	if(progress==0)
    		return true;
    	else
    		return false;
    }
    
    public void onComplete() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Progress Completed"));
    }
     
    public void cancel() {
        progress = null;
    }
}