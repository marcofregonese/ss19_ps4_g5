package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.PoolService;
import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.services.AnswerStatisticService;
import at.qe.sepm.skeleton.services.UserService;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("view")
public class UserProfileListController {


		 @Autowired
		    private UserService userService;
		 
		    
		
		 public Collection<User> getUsers() {
		        return userService.getAllUsers();
		    }


}

	
	
	
	
	
	
	

