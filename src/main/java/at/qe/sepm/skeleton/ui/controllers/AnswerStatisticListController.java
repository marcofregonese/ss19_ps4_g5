package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.PoolService;
import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.services.AnswerStatisticService;
import at.qe.sepm.skeleton.services.UserService;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Controller for the user list view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")

public class AnswerStatisticListController {


	 @Autowired
	    private UserService userService;
	 
	    
	
	 public Collection<User> getUsers() {
	        return userService.getAllUsers();
	    }


}
