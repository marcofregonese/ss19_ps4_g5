package at.qe.sepm.skeleton.ui.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.services.AnswerService;
import at.qe.sepm.skeleton.services.PoolService;
import at.qe.sepm.skeleton.services.QuestionService;

@Component
@Scope("view")

public class QuestionDetailController {

	@Autowired
	private QuestionService questionService;

	private Question question;
	
	/**
	 * Sets the currently displayed user and reloads it form db. This user is
	 * targeted by any further calls of
	 * {@link #doReloadUser()}, {@link #doSaveUser()} and
	 * {@link #doDeleteUser()}.
	 *
	 * @param user
	 */
	public void setQuestion(Question q) {
		this.question=q;
		doReloadQuestion();
	}
	
	 public void doReloadQuestion() {
	        question = questionService.findById(question.getId());
	    }

	/**
	 * Returns the currently displayed user.
	 *
	 * @return
	 */
	public Question getQuestion() {
		return question;
	}

	public Question randomQuestion() {
		return questionService.getRandomQuestion().get(0);
		
		
	}

	/**
	 * Action to save the currently displayed user.
	 */
	public void doSaveQuestion() {
		question = this.questionService.saveQuestion(question);
	}

	/**
	 * Action to delete the currently displayed user.
	 */
	
	public String randomAnswer() {
		return this.questionService.getRandomAnswer().get(0).getAnswer();
	}
	

}
