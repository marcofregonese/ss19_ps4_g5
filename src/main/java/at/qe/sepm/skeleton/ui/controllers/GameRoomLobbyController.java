package at.qe.sepm.skeleton.ui.controllers;


import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.GameRoom;

import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.GameRoomService;
import at.qe.sepm.skeleton.services.PoolService;
import at.qe.sepm.skeleton.services.QuestionService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.push.EventBusFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */

@Component
@Scope("view")
 

public class GameRoomLobbyController {


	@Autowired
	private GameRoomService gameRoomService;
	@Autowired
	private PoolService poolService = new PoolService();
	@Autowired
	private UserService userService = new UserService();
	@Autowired
	private SessionInfoBean sessionInfoBean;
	@Autowired
	private QuestionService questionService;

	
	
	private GameRoom gameRoom=new GameRoom();
	private long grid;
	private List<Pool> poollist = new ArrayList<Pool>();
	private User user;
	private int gameId;
	
	 @PostConstruct
	    public void Init() {
		 	user=sessionInfoBean.getCurrentUser();
		 	this.gameRoom=gameRoomService.getByUser(user);
		 	this.setGameId(gameRoom.getId().intValue());
	        
	    }
	
	public void setGameRoom(GameRoom gameRoom) {
		this.gameRoom = gameRoom;
	}

	
	public GameRoom getGameRoom() {
		return gameRoom;
	}

	public void setPoolList(List<Pool> poollist) {
		this.poollist = poollist;
		this.gameRoom.setPools(poollist);
		this.gameRoomService.saveGameRoom(gameRoom);
		System.out.println(poollist);
	}

	public List<Pool> getPoolList(){
		return poollist;
	}


	public void setGrid(long grid) {
		this.grid = grid;
	}
	public long getGrid() {
		return this.grid;
	}


	
	public List<User> getGameroomUsers(){
		return this.gameRoom.getUsers();
	}

	public boolean isTeamleader() {
		this.user=sessionInfoBean.getCurrentUser();

		if(this.user.getRoles().contains(UserRole.TEAMLEADER)) {
			return true;
		}

		else 
			return false;
			
	}

	public String redirectToGame() {
		return "game?faces-redirect=true&includeViewParams=true";
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

}

