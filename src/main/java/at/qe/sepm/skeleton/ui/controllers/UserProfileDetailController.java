
package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.repositories.UserRepository;
import at.qe.sepm.skeleton.services.AnswerStatisticService;
import at.qe.sepm.skeleton.services.QuestionService;
import at.qe.sepm.skeleton.services.UserService;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("view")
public class UserProfileDetailController {


	@Autowired
	private UserService userService;

	private User user;
	private User currentUser;
	boolean check = true;


	public User getCurrentUser() {
		if(check){
			check = false;
			this.currentUser = userService.getAuthenticatedUser();
		}
		return currentUser;
	}


	public User loadCurrentUser() {
		return userService.getAuthenticatedUser();
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
		doReloadUser();
	}

	public void doReloadUser() {
		user = userService.loadUser(user.getUsername());
	}
}
