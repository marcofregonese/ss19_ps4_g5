package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import at.qe.sepm.skeleton.model.UserProfile;
import at.qe.sepm.skeleton.services.UserProfileService;
/**
 * Controller for the user detail view.
 *
 * This class is part of the skeleton project provided for students of the
 * course "Softwaredevelopment and Project Management" offered by the University
 * of Innsbruck.
 */
@Component
@Scope("view")
public class UserDetailController {
	

	@Autowired
	BCryptPasswordEncoder encoder;	
	
    @Autowired
    private UserService userService;
    @Autowired
    private UserProfileService userProfileService;
    @Autowired
    private SessionInfoBean session;

    /**
     * Attribute to cache the currently displayed user
     */
    private User user;
    private User newUser = new User();
    private UserRole chooseRole;
    
    
    
    public boolean isAdmin(){
    	if(session.getCurrentUser().getRole().equals(UserRole.ADMIN))
    		return false;
    	else
    		return true;
    }
    
    
    
    public User getNewUser() {
		return newUser;
	}

	public void setNewUser(User newUser) {
		this.newUser = newUser;
	}

	
	public UserRole getChooseRole() {
		return chooseRole;
	}

	public void setChooseRole(UserRole chooseRole) {
		Set<UserRole> roles = new HashSet<>();
		roles.add(chooseRole);
		this.newUser.setRoles(roles);
	}

	public void doCreateUser() {
		this.newUser.setEnabled(true);
		this.newUser.setPassword(encoder.encode(newUser.getPassword()));
        this.user=this.userService.saveUser(newUser);
        UserProfile profile = new UserProfile();
        profile.setNoFailed(0);
        profile.setNoSucceed(0);
        profile.setPlayedWith("");
        profile.setRightPools(0);
        profile.setTotalTime(0);
        profile.setOwner(user);
        userProfileService.saveUserProfile(profile);

    }
	
//	@RequestMapping(value="/login", method = RequestMethod.GET)
//	public ModelAndView showRegistrationPage(ModelAndView modelAndView, User user){
//		modelAndView.addObject("user", user);
//		modelAndView.setViewName("register");
//		return modelAndView;
//	}

	/**
     * Sets the currently displayed user and reloads it form db. This user is
     * targeted by any further calls of
     * {@link #doReloadUser()}, {@link #doSaveUser()} and
     * {@link #doDeleteUser()}.
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
        doReloadUser();
    }

    /**
     * Returns the currently displayed user.
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     * Action to force a reload of the currently displayed user.
     */
    public void doReloadUser() {
        user = userService.loadUser(user.getUsername());
    }

    /**
     * Action to save the currently displayed user.
     */
    public void doSaveUser() {
        user = this.userService.saveUser(user);
    }

    /**
     * Action to delete the currently displayed user.
     */
    public void doDeleteUser() {
        this.userService.deleteUser(user);
        user = null;
    }

}
