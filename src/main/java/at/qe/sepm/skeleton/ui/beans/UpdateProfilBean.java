/**
 * 
 */
package at.qe.sepm.skeleton.ui.beans;

import javax.annotation.PostConstruct;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import at.qe.sepm.skeleton.validator_imported.EmailValidator;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.UserService;

/**
 * @author Lukas
 *
 */

@Component
@Scope("view")
public class UpdateProfilBean {
	
	 @Autowired
	 private UserService userService;
	
	 private User user;
	
	 private String firstName;
	 
	 private String lastName;
	 
	 private String email;
	 
	 private boolean vaildEMail;
	 
	 private String loadedEmail;
	 
	 private String phone;
	 
	 private boolean vaildPhone;
	 
	 private String loadedPhone;
	
	 /**
	  * Help functions checks if firstname, lastname, email and 
	  * phone are all set and not empty and returns an array of according integer values.
	  * The pre-last value indicates if all of them are true, the last how many
	  * @return
	  */
	@SuppressWarnings("unused")
	private int[] valueCheck() {
		int[] r=new int[6];
		r[5]=0;
		r[0]=(firstName==null || firstName.isEmpty())?0:1; r[4]=r[0]; r[5]+=r[0];
		r[1]=(lastName==null || lastName.isEmpty())?0:1;  r[4]=Integer.min(r[1],  r[4]); r[5]+=r[1];
		r[2]=(email==null || email.isEmpty())?0:1;  r[4]=r[4]=Integer.min(r[1],  r[2]); r[5]+=r[2];
		r[3]=(phone==null || phone.isEmpty())?0:1;  r[4]=Integer.min(r[1],  r[4]); r[5]+=r[3];
		return r;
	
	}
	
	
	
	@PostConstruct
	public void setAuthenticatedUser() {
		user = userService.getAuthenticatedUser();
		firstName=user.getFirstName();
		lastName=user.getLastName();
		email=user.getEmail();
		loadedEmail=user.getEmail();
		vaildEMail=true;
		phone=user.getPhone();
		loadedPhone=user.getPhone();
		vaildPhone=true;
	}
	
	public void updateProfile() {
		if((firstName==null || firstName.isEmpty())) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR",
					"Please provide a firstname");
			RequestContext.getCurrentInstance().showMessageInDialog(message);
		}else if(!vaildEMail) {
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmail(loadedEmail);
			user.setPhone(phone);
			user = userService.saveUser(user);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Your profile has been successfully updated,"
							+ " however your new E-Mail address was invaild, your old one was kept!"));
		
		}else {
			String sphone=(vaildPhone)?phone:loadedPhone;
			String semail=(vaildEMail)?email:loadedEmail;
			String message="Your profile has been successfully updated";
			if(!vaildPhone || !vaildEMail) {
				if(!vaildPhone && !vaildEMail) {
					message+=" however your new Phone and E-Mail address was invaild, your old ones were kept";
				}else if(!vaildPhone) {
					message+=" however your new Phone address was invaild, your old one was kept";
				}else {
					message+=" however your new E-Mail address was invaild, your old one was kept";
				}
			}
			loadedPhone=sphone;
			phone=sphone;
			email=semail;
			loadedEmail=semail;
			message+="!";
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmail(semail);
			user.setPhone(sphone);
			user = userService.saveUser(user);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
		
		}
	}
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}


	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}


	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		vaildEMail=EmailValidator.getInstance(true, true).isValid(email);
		this.email = email;
	}


	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}


	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		vaildPhone=phone.isEmpty() || phone.substring(1).matches("[0-9]+$");
		this.phone = phone;
	}


	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
}
