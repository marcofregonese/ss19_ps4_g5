package at.qe.sepm.skeleton.ui.controllers;


import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.GameMode;
import at.qe.sepm.skeleton.model.GameRoom;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.AnswerService;
import at.qe.sepm.skeleton.services.AnswerStatisticService;
import at.qe.sepm.skeleton.services.GameRoomService;
import at.qe.sepm.skeleton.services.QuestionService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.Game;
import at.qe.sepm.skeleton.ui.beans.GameLogic;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;


import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import at.qe.sepm.skeleton.model.UserProfile;
import at.qe.sepm.skeleton.services.UserProfileService;
@Component
@Scope("view")
public class GameController{


	@Autowired
	private SessionInfoBean sessionInfoBean;
	@Autowired
	private AnswerStatisticService statistics;
	@Autowired
	private GameRoomService gameroomService;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserProfileService userProfileService;



	private Integer progress;
	private int gameId;
	private GameRoom gameroom;
	private User user;

	private Question question;
	private List<Answer> answers;

	private Answer answer;
	private List<Question> questions;

	private EventBus eventBus;
	private boolean gameover=false;

	@Autowired
	private GameLogic gameLogic;

	private Game game;
	boolean answersSet=false;
	private int var=0;
	private byte[] picture;

	public Answer test;

	public Answer getTest() {
		return test;
	}

	public void setTest(Answer test) {
		this.test = test;
	}

	@PostConstruct
	public void Init() {
		this.eventBus=EventBusFactory.getDefault().eventBus();
		this.user=sessionInfoBean.getCurrentUser();
		if(gameroomService.getByUser(user)!=null) {
			this.gameroom=gameroomService.getByUser(user);
			this.gameId=gameroom.getId().intValue();
			this.game=gameLogic.getGames().get(gameId);
			if(game.getUserQuestions().get(user)==null) {
				question = new Question();
				answers=new ArrayList<Answer>();
			}
			else {
				question=game.getUserQuestions().get(user);
				answers=game.getUserAnswers().get(user);
			}
		}
		else
			redirect("/secured/welcome.xhtml");

	}




	public void preRender() {
		this.user=sessionInfoBean.getCurrentUser();

		if(gameroomService.getByUser(user)==null)
			redirect("/secured/welcome.xhtml");
	}

	@PreDestroy
	public void destroy() {
		if(gameroom.getUsers().get(0).getRoles().contains(UserRole.TEAMLEADER)) {
			User u=gameroom.getUsers().get(0);
			u.getRoles().remove(UserRole.TEAMLEADER);
		}

	}

	public void redirectForPlayers(){

		game=this.gameLogic.getGames().get(gameId);


		question=game.getUserQuestions().get(user);
		answers=game.getUserAnswers().get(user);



		answersSet=true;

		redirect("/user/game.xhtml");
	}



	public void gameOver() throws IOException {
		if(gameover) {
			AnswerStatistic stats=new AnswerStatistic();
			List<User> userList = game.getUsers();
			UserProfile profile;
			profile = user.getProfile();
			userProfileService.deleteProfile(user.getProfile());


			for (User user2 : userList) {
				if(!(user.getUsername().equals(user2.getUsername()))&& !(profile.getPlayedWith().contains(user2.getUsername()))){
					if(profile.getPlayedWith().equals("")){
						profile.setPlayedWith(user2.getUsername());
					}
					else {
						String username = user2.getUsername();
						username += ", " + profile.getPlayedWith();
						profile.setPlayedWith(username);
					}
				}
			}
			profile.setNoSucceed(profile.getNoSucceed() + game.getUserRightAnswers().get(user));
			profile.setNoFailed(profile.getNoFailed() + game.getUserWrongAnswers().get(user));
			if(game.getUserWrongAnswers().get(user) == 0){
				profile.setRightPools(1 + profile.getRightPools());
			}
			profile.setTotalTime(profile.getTotalTime() + System.currentTimeMillis() - game.getTime());
			profile.setOwner(user);
			profile=userProfileService.saveUserProfile(profile);
			user.setProfile(profile);
			user=userService.saveUser(user);


			stats.setPlayer(user);
			stats.setNoSucceed(game.getUserRightAnswers().get(user));
			stats.setNoFailed(game.getUserWrongAnswers().get(user));
			double right = game.getUserRightAnswers().get(user);
			double wrong = game.getUserWrongAnswers().get(user);
			stats.setPercentage(right/(right+wrong) * 100);
			stats.setTopic(game.getPools().get(game.getCurrentPoolId()).getTopic());
			statistics.saveAnswerStatistic(stats);

			if(this.gameroom!=null){
				if(gameroom.getUsers().get(0).getRoles().contains(UserRole.TEAMLEADER)) {
					User u=gameroom.getUsers().get(0);
					u.getRoles().remove(UserRole.TEAMLEADER);
				}

			}

			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			ec.redirect(ec.getRequestContextPath() +"/secured/welcome.xhtml");
		}
	}


	public void update(){
		game=gameLogic.getGames().get(gameId);

		question=game.getUserQuestions().get(user);
		answers=game.getUserAnswers().get(user);

		boolean gameBlocked=true;
		for(User u : game.getUsers()) {
			if(game.getUserBlocked().get(u)==false)
				gameBlocked=false;
		}
		if(gameBlocked==true) {

			gameover=true;

		}

	}

	public void doAnswer(Answer answer) {
		game=gameLogic.getGames().get(gameId);
		answersSet=false;
		int questionId=game.questionAnswered(question,user);
		boolean answeredRight=false;
		for(User u:game.getUsers()) {
			if(!game.getUserQuestions().get(u).getQuestion().equals("")) {
				Question userQuestion=game.getUserQuestions().get(u);
				if(userQuestion.getAnswers().contains(answer)) {
					if(userQuestion.getAnswers().get(0).equals(answer))
						answeredRight=true;
				}
			}
		}

		game.changeStatistic(user,answeredRight);//if answeredRight is true nrRightAnswers goes up
		if(questionId!=-1){//the next lines are for a normal game where the player has answers for his own question
			game.setBlocked(true);
			gameLogic.updateGame(gameId, game);
			game.reorderAnswers(user,questionService.findById(questionId),answer);
			question=game.getUserQuestions().get(user);
			answers=game.getUserAnswers().get(user);
			answersSet=true;
			game.setBlocked(false);
			gameLogic.updateGame(gameId, game);

			this.progress=20;
		}
		else{
			Question userQuestion;
			List<Answer> answersForRemovedQuestion=new ArrayList<Answer>();
			if(game.getGameMode().equals(GameMode.QUESTION)) {
				for(User u:game.getUsers()) {
					if(!game.getUserQuestions().get(u).getQuestion().equals("")) {
						userQuestion=game.getUserQuestions().get(u);
						if(userQuestion.getAnswers().contains(answer)) {
							if(userQuestion.getAnswers().contains(answer)) {
								answersForRemovedQuestion=userQuestion.getAnswers();
								Question q = new Question();
								q.setQuestion("");
								game.getUserQuestions().put(u, q);
								break;
							}
						}
					}
				}

				for(User u : game.getUsers()) {
					for(int i=0;i<game.getUserAnswers().get(u).size();i++) {
						Answer a =new Answer();
						a.setAnswer("");
						if(answersForRemovedQuestion.contains(game.getUserAnswers().get(u).get(i)))
							game.getUserAnswers().get(u).set(i, a);
					}
				}
			}
			game.getUserBlocked().put(user, true);
			this.gameLogic.updateGame(gameId, game);

		}
		eventBus.publish("/game/" + gameId, 1);

	}

	public void joker() {
		game=gameLogic.getGames().get(gameId);
		game.setJokerUsed(true);
		game.initializeAfterJoker();
		this.gameLogic.updateGame(gameId, game);
		eventBus.publish("/game/" + gameId, 1);
	}

	public boolean jokerAvailable() {
		if(game.isJokerUsed())
			return false;
		else
			return true;
	}

	public int getVar() {
		return var;
	}

	public void setVar(int var) {
		this.var = var;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public String getStatsCurrentPool() {
		String ret="Stats: "+gameLogic.getGames().get(gameId).getUserRightAnswers().get(user)+"/"+(gameLogic.getGames().
				get(gameId).getUserRightAnswers().get(user)+gameLogic.getGames().get(gameId).getUserWrongAnswers().get(user));
		return ret;
	}

	public void redirect(String url) {
		try{
			FacesContext fc=FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(url);
			fc.responseComplete();
			fc.renderResponse();
			return;
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	public boolean questionMode() {
		if(game.getGameMode().equals(GameMode.QUESTION))
			return true;
		else
			return false;
	}

	public boolean answerMode() {
		if(game.getGameMode().equals(GameMode.ANSWER)) 
			return true;
		else
			return false;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public User getUser() {
		return user;
	}

	public void user(User currentUser) {
		this.user = currentUser;
	}


	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}
	public Integer getProgress() {
		if(progress==null)
			progress=20;
		return progress;
	}

	public void decrement() {
		this.progress--;
	}

	public boolean stop() {
		if(this.progress==0) {
			eventBus.publish("/game/" + gameId, 1);
			timeOver();
			return true;
		}
		return false;
	}

	private void timeOver() {
		game=gameLogic.getGames().get(gameId);
		Question question=game.getUserQuestions().get(user);
		doAnswer(question.getAnswers().get(1));
	}


	public void cancel() {
		progress = null;
	}

	public void startGame() {
		game=gameLogic.getGames().get(gameId);
		game.initialize();
		game.setTime(System.currentTimeMillis());
		this.eventBus.publish("/game/"+gameId, 1);
		this.gameLogic.addGame(gameId, game);
	}

	public String leaveGame() {
		user.setGameRooms(null);
		game=gameLogic.getGames().get(gameId);
		game.removeUser(user);
		this.gameLogic.updateGame(gameId, game);
		userService.saveUser(user);
		return "secured/welcome.xhtml?faces-redirect=true";
	}

	public String answerRev() {
		return question.getAnswers().get(0).getAnswer();
	}

	public boolean isGameover() {
		return gameover;
	}

	public void setGameover(boolean gameover) {
		this.gameover = gameover;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	public Answer getAnswer() {
		return answer;
	}


	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}


}
