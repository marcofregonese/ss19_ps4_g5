package at.qe.sepm.skeleton.ui.controllers;


import org.primefaces.push.EventBus;
import org.primefaces.push.RemoteEndpoint;
import org.primefaces.push.annotation.*;
import org.primefaces.push.impl.JSONDecoder;
import org.primefaces.push.impl.JSONEncoder;
import org.springframework.beans.factory.annotation.Autowired;

import at.qe.sepm.skeleton.ui.beans.GameLogic;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;

@PushEndpoint("/game/{gameId}")
public class Endpoints
{
	@PathParam("{gameId}")
    private String gameId;
	
	@Autowired
	private GameLogic gameLogic;
	@Autowired
	private SessionInfoBean sessionInfoBean;
	
	@OnOpen
	public void onOpen(RemoteEndpoint r, EventBus eventBus)
	{
		System.out.println("Opened connection");
	}

	@OnMessage(decoders = {JSONDecoder.class}, encoders = {JSONEncoder.class})
	public String onMessage(String msg) {
		System.out.println(msg);
		return msg;
	}



	@OnClose
	public void onClose()
	{
		//gameLogic.getGames().get(Integer.parseInt(gameId)).removeUser(sessionInfoBean.getCurrentUser());
		System.out.println("Connection closed");//- User " + sessionInfoBean.getCurrentUser().getUsername() + "left");
	}

	
}



