package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.PoolService;
import at.qe.sepm.skeleton.services.UserService;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("view")

public class PoolListController {


	@Autowired
	private PoolService poolService;
	@Autowired
	private UserService userService;

	/**
	 * Returns a list of all users.
	 *
	 * @return
	 */
	public Collection<Pool> getPools() {
		return poolService.getAllPools();
	}
	
	public List<Pool> getMyPools(){
		User user = userService.getAuthenticatedUser();
		return user.getPool();
	}


}
