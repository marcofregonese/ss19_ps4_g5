
package at.qe.sepm.skeleton.services; 

import java.util.Collection;
import java.util.Date;

import at.qe.sepm.skeleton.repositories.GameRoomRepository; 

import org.springframework.beans.factory.annotation.Autowired; 

import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component; 

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import at.qe.sepm.skeleton.model.GameRoom;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;

@Component 
@Scope("application") 
public class GameRoomService { 

 	 @Autowired 
	 private GameRoomRepository gameroomRepository; 

 	 public List<Question> getQuestion(Pool pool){ 
	 	 return gameroomRepository.getQuestion(pool); 
 	 } 

 	 public Collection<GameRoom> getByGameRoomSate(String gameRoomSate){ 
	 	 return gameroomRepository. getByState(gameRoomSate); 
 	 } 

 	 public Collection<GameRoom> getByStartDateBetween(LocalDate startDate, LocalDate endDate){ 
	 	 return gameroomRepository. getByStartDateBetween(startDate, endDate); 
 	 } 

 	 public Collection<GameRoom> getByEndDateBetween(LocalDate startDate, LocalDate endDate){ 
	 	 return gameroomRepository. getByEndDateBetween(startDate, endDate); 
 	 } 

 	 public Collection<GameRoom> getByStartTimeBetween(LocalTime startTime, LocalTime endTime){ 
	 	 return gameroomRepository. getByStartTimeBetween(startTime, endTime); 
 	 } 

 	 public Collection<GameRoom> getByEndTimeBetween(LocalTime startTime, LocalTime endTime){ 
	 	 return gameroomRepository. getByEndTimeBetween(startTime, endTime); 
 	 } 

     
     public GameRoom saveGameRoom(GameRoom gameRoom) {
         return gameroomRepository.save(gameRoom);
     }

	public GameRoom getGame(User user) {
		return gameroomRepository.getByUser(user);
	}

	public GameRoom getById(long id) {
		return gameroomRepository.getById(id);
	}

	public GameRoom getByUser(User user) {
		return gameroomRepository.getByUser(user);
	}

	public void deleteGameRoom(GameRoom gameRoom) {
		gameroomRepository.delete(gameRoom);
		
	}




 }