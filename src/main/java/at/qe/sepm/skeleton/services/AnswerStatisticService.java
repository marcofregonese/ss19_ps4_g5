/** 
* @author: Michael 
* 
*/ 
package at.qe.sepm.skeleton.services; 

import java.util.Collection; 

import at.qe.sepm.skeleton.repositories.AnswerStatisticRepository; 

import org.springframework.beans.factory.annotation.Autowired; 

import org.springframework.context.annotation.Scope; 

import org.springframework.stereotype.Component; 

import java.time.Duration;
import java.util.List;

import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.GameRoom;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;

@Component 
@Scope("application") 
public class AnswerStatisticService { 

 	 @Autowired 
	 private AnswerStatisticRepository answerstatisticRepository; 

 	 public AnswerStatistic  getStatisticById(long id){ 
	 	 return answerstatisticRepository.  getStatisticById(id); 
 	 } 

 	 public AnswerStatistic  getStatisticByPlayer(User player){ 
	 	 return answerstatisticRepository.getStatisticByPlayer(player); 
 	 } 

 	 public Collection<AnswerStatistic> findAll(){ 
	 	 return answerstatisticRepository. findAll(); 
 	 } 

 	public List<AnswerStatistic> getAnswerStatistics(User currentUser) {
 		return answerstatisticRepository.getStatistics(currentUser);
 	}

	public AnswerStatistic saveAnswerStatistic(AnswerStatistic stats) {
		return answerstatisticRepository.save(stats);
		
	}

 }