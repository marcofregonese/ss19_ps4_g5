package at.qe.sepm.skeleton.services; 

import java.util.Collection; 

import at.qe.sepm.skeleton.repositories.QuestionRepository; 

import org.springframework.beans.factory.annotation.Autowired; 

import org.springframework.context.annotation.Scope; 

import org.springframework.stereotype.Component; 

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;

@Component 
@Scope("application") 
public class QuestionService { 

	@Autowired 
	private QuestionRepository questionRepository; 

	public Question findById(long id){ 
		return questionRepository. findById(id); 
	} 


	public Collection<Question> findAll(){ 
		return questionRepository. findAll(); 
	} 

	public Collection<Question> findByQuestionContaining(String question){ 
		return questionRepository. findByQuestionContaining(question); 
	} 

	public Collection<Question> findByTopic(String topic){ 
		return questionRepository. findByTopic(topic); 
	} 

	public Collection<Question> findByDifficulty(String difficulty){ 
		return questionRepository. findByDifficulty(difficulty); 
	} 

	//PORCO DIO REPORT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	public Collection<Question> findByPoolTag(Pool pool){ 
		return questionRepository. findByPoolTag(pool); 
	}
	//PORCO DIO REPORT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	public List<Question> findByPool(Pool pool) {
		return questionRepository.findById(pool);

	}

	public Question saveQuestion(Question question) {
		return questionRepository.save(question);
	}

	public void deleteQuestion(Question question) {
		questionRepository.delete(question);

	}

//	public void deleteQuestionqq(Long question) {
//		questionRepository.deleteQ(question);
//
//	}

	public List<Question> getRandomQuestion() {
		return questionRepository.getRandomQuestion();
	} 

	public List<Answer> getRandomAnswer() {
		return questionRepository.getRandomAnswer();
	} 


}