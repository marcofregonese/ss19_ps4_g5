/** 
* @author: Lukas 
* 
*/ 
package at.qe.sepm.skeleton.services; 

import java.util.Collection; 

import at.qe.sepm.skeleton.repositories.AnswerRepository;
import at.qe.sepm.skeleton.repositories.AnswerStatisticRepository;

import org.springframework.beans.factory.annotation.Autowired; 

import org.springframework.context.annotation.Scope; 

import org.springframework.stereotype.Component; 

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;

@Component 
@Scope("application") 
public class AnswerService { 

 	 @Autowired 
	 private AnswerRepository answerRepository; 
  	 
 	 public Answer findById(long id){ 
	 	 return answerRepository. findById(id); 
 	 } 

 	 public Collection<Answer> findAll(){ 
	 	 return answerRepository. findAll(); 
 	 } 

 	 public Collection<Answer> findByAnswerContaining(String answer){ 
	 	 return answerRepository. findByAnswerContaining(answer); 
 	 } 

 	 public Collection<Answer> findByTopic(String topic){ 
	 	 return answerRepository. findByTopic(topic); 
 	 } 

 	 public Collection<Answer> findByDifficulty(String difficulty){ 
	 	 return answerRepository. findByDifficulty(difficulty); 
 	 } 

 	 public Collection<Answer> findByPoolTag(String pool){ 
	 	 return answerRepository. findByPoolTag(pool); 
 	 } 
 	 
 	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TEACHER')")
 	public Answer saveAnswer(Answer answer) {
        return answerRepository.save(answer);
    }


 }