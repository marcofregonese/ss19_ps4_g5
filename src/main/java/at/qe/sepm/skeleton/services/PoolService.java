package at.qe.sepm.skeleton.services; 

import java.util.Collection;
import java.util.Date;

import at.qe.sepm.skeleton.repositories.PoolRepository; 

import org.springframework.beans.factory.annotation.Autowired; 

import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component; 

import java.time.LocalTime;
import java.util.List;

import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.User;

@Component 
@Scope("application") 
public class PoolService { 

 	 @Autowired 
	 private PoolRepository poolRepository; 

 	 public Pool getByID(long id){ 
	 	 return poolRepository. getById(id); 
 	 } 

// 	 public Collection<Pool> getByDifficutlyBetween(int min, int max){ 
//	 	 return poolRepository. getByDifficultyBetween(min, max); 
// 	 } 

 	 public Collection<Pool> getByTopic(String topic){ 
	 	 return poolRepository. getByTopic(topic); 
 	 } 

 	 public Collection<Pool> getByStartTimeBetween(LocalTime a, LocalTime b){ 
	 	 return poolRepository. getByStartTimeBetween(a, b); 
 	 } 

 	 public Collection<Pool> getByEndTimeBetween(LocalTime a, LocalTime b){ 
	 	 return poolRepository. getByEndTimeBetween(a, b); 
 	 } 

 	 public Collection<Pool> getByTotalPointsBetween(int a, int b){ 
	 	 return poolRepository. getByTotalPointsBetween(a, b); 
 	 }

	public Collection<Pool> getAllPools() {
		return poolRepository.findAll();
	}

	  @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TEACHER')")
    public Pool savePool(Pool pool) {
        return poolRepository.save(pool);
    }

    /**
     * Deletes the user
     *
     * @param user the user to delete
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    public void deletePool(Pool pool) {
        poolRepository.delete(pool);
        // :TODO: write some audit log stating who and when this user was permanently deleted.
    }


 }