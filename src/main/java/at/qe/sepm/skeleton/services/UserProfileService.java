package at.qe.sepm.skeleton.services; 

import java.util.Collection; 
import at.qe.sepm.skeleton.model.UserProfile;

import at.qe.sepm.skeleton.repositories.AnswerStatisticRepository;
import at.qe.sepm.skeleton.repositories.UserProfileRepository;

import org.springframework.beans.factory.annotation.Autowired; 

import org.springframework.context.annotation.Scope; 

import org.springframework.stereotype.Component; 

import java.time.Duration;
import java.util.List;

import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;

@Component 
@Scope("application") 
public class UserProfileService { 

 	 @Autowired 
	 private UserProfileRepository userProfileRepository; 

 	public UserProfile saveUserProfile(UserProfile profile) {
		return userProfileRepository.save(profile);
	}
 	
 	public UserProfile findById(long id){ 
		return userProfileRepository.findById(id); 
	} 
 	
 	public void deleteProfile(UserProfile profile) {
		userProfileRepository.delete(profile);

	}


 }