/**
 * 
 */
package at.qe.sepm.skeleton.repositories;

import java.time.Duration;
import at.qe.sepm.skeleton.model.UserProfile;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import at.qe.sepm.skeleton.model.AnswerStatistic;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;


public interface UserProfileRepository extends AbstractRepository<UserProfile, Long> {

	UserProfile findById(long id);
}
