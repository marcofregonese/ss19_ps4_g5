/**
 * 
 */
package at.qe.sepm.skeleton.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;

/**
 * @author marco
 *
 */
public interface AnswerRepository extends AbstractRepository<Answer, Long>{
	
	Answer findById(long id);
	
	List<Answer> findAll();

	List<Answer> findByAnswerContaining(String answer);

	List<Answer> findByTopic(String topic);

	List<Answer> findByDifficulty(String difficulty);
	
	List<Answer> findByPoolTag(String pool);
	
	
}