/**
 * 
 */
package at.qe.sepm.skeleton.repositories;

import java.time.LocalTime;
import java.util.List;

import at.qe.sepm.skeleton.model.Pool;

/**
 * @author marco
 *
 */
public interface PoolRepository extends AbstractRepository<Pool, Long> {
    Pool getById(long id);
    
    List<Pool> getByDifficultyBetween(int min, int max);
    
    List<Pool> getByTopic(String topic);
    
    List<Pool> getByStartTimeBetween(LocalTime a, LocalTime b);
    
    List<Pool> getByEndTimeBetween(LocalTime a, LocalTime b);
    
    List<Pool> getByTotalPointsBetween(int a, int b);
    
 
    
    
}
