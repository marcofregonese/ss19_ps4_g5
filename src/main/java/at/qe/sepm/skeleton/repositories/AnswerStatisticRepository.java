/**
 * 
 */
package at.qe.sepm.skeleton.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import at.qe.sepm.skeleton.model.AnswerStatistic;

import at.qe.sepm.skeleton.model.User;


/**
 * @author marco
 *
 */
public interface AnswerStatisticRepository extends AbstractRepository<AnswerStatistic, Long> {
	AnswerStatistic  getStatisticById(long id);
	
	AnswerStatistic  getStatisticByPlayer(User player);
	
	List<AnswerStatistic> findAll();
	
	List<AnswerStatistic> findByNoSucceedBetween(int min, int max);
	
	List<AnswerStatistic> findByNoFailedBetween(int min, int max);
	
	
	@Query("SELECT (cast(SUM(a.noSucceed) as double)/cast(SUM(a.noSucceed)+SUM(a.noFailed) as double)) * 100.0 as percentage,a.topic, SUM(a.noSucceed), SUM(a.noFailed) FROM AnswerStatistic a WHERE :user = a.player group by a.topic order by percentage desc")
	List<AnswerStatistic> getStatistics(@Param("user") User user);
}
