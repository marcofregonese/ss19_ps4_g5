/**
 * 
 */
package at.qe.sepm.skeleton.repositories;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import at.qe.sepm.skeleton.model.GameRoom;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;

/**
 * @author marco
 *
 */
public interface GameRoomRepository extends AbstractRepository<GameRoom, Long>{
	
	GameRoom getById(long id);
	
	List<GameRoom> getByState(String gameRoomSate);
	
	List<GameRoom> getByStartDateBetween(LocalDate startDate, LocalDate endDate);
	
	List<GameRoom> getByEndDateBetween(LocalDate startDate, LocalDate endDate);
	
	List<GameRoom> getByStartTimeBetween(LocalTime startTime, LocalTime endTime);
	
	List<GameRoom> getByEndTimeBetween(LocalTime startTime, LocalTime endTime);

	@Query("SELECT u.gameRooms FROM User u WHERE :user = u")
	GameRoom getByUser(@Param("user") User user);
	
	 @Query("SELECT q FROM Question q WHERE :pool = q.poolTag")
	 List<Question> getQuestion(@Param("pool") Pool pool);

}
