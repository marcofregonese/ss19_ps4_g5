/**
 * 
 */
package at.qe.sepm.skeleton.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import at.qe.sepm.skeleton.model.Answer;
import at.qe.sepm.skeleton.model.Pool;
import at.qe.sepm.skeleton.model.Question;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;

/**
 * @author marco
 * @coauthor Lukas
 * A respotory provides getters from the database, for a spefic enity, in this case question.
 */
public interface QuestionRepository extends AbstractRepository<Question, Long>{
	
	Question findById(long id);
	
	List<Question> findAll();
	
	List<Question> findByQuestionContaining(String question);
	
	List<Question> findByTopic(String topic);
	
	List<Question> findByDifficulty(String difficulty);
	
	List<Question> findByPoolTag(Pool pool);
	
    @Query("SELECT q FROM Question q WHERE :pool = q.poolTag")
    List<Question> findById(@Param("pool") Pool pool);
    
    @Query("SELECT a FROM Answer a order by rand()")
	List<Answer> getRandomAnswer();

    @Query("SELECT q FROM Question q")
	List<Question> getRandomQuestion();
    
    @Query("delete from Question q WHERE q.id = :entityId")
    void deleteQ(Long entityId);
		
}