### 2019 project for "Software development"

- Language: Java
- Framework: Spring
- DB: MySQL

### Requirements: Install Vagrant for your OS.

To run the system: Open a terminal in the folder with vagrantfile. Execute the command "vagrant up". After the compilation of the program type in your browser "localhost:9000" to run the system. On other devices you need to type "My IP-Adress:9000" to run the system. For getting the IP-Adress open a terminal and type in one of the following commands: ifconfig for Linux, ipconfig for Windows and ipconfig getifaddr en0 (wireless) or ipconfig getifaddr en1 (Ethernet) on MacOS. After that you can see the corresponding IP-Adress. 